import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MainService } from '../../services/main.service';

import * as $ from "jquery";

interface HtmlInputEvent extends Event {
  target : HTMLInputElement & EventTarget;
}

@Component({
  selector: 'app-create-new',
  templateUrl: './create-new.component.html',
  styleUrls: ['./create-new.component.scss']
})
export class CreateNewComponent implements OnInit {
  public myGroup! : FormGroup;
  public newsMainInfoForm! : FormGroup;
  public disableButton = true;
  public file! : File;
  public currentUserId: any = this.main.getCurrentUser().currentUser.id;
  private editNews : any = this.main.getParamsNews();

  /* The configuration of the editor. */
  public editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '35em',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
    {class: 'arial', name: 'Arial'},
    {class: 'times-new-roman', name: 'Times New Roman'},
    {class: 'calibri', name: 'Calibri'},
    {class: 'comic-sans-ms', name: 'Comic Sans MS'}
    ],
    customClasses: [
    {
      name: 'Title',
      class: 'titleText',
      tag: 'h1',
    },
    {
      name: 'Sub Title',
      class: 'subTitleText',
      tag: 'h2',
    }
    ],
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
    ['bold', 'italic'],
    ['insertVideo','link','unlink','superscript',
    'subscript','removeFormat','insertHorizontalRule', 'backgroundColor']
    ],
  };


  constructor(
    private formBuilder : FormBuilder,
    public main : MainService
    ) { }

  /**
   * If the news variable is undefined, then the form is initialized with empty values. If the news
   * variable is not undefined, then the form is initialized with the values of the news variable.
   */
  ngOnInit(): void {

    let news = this.main.getParamsNews();

    if(news == undefined){
      this.myGroup = this.formBuilder.group({
        htmlContent1 : new FormControl('',[Validators.required, Validators.minLength(450)]),
      });

      this.newsMainInfoForm = this.formBuilder.group({
        title : new FormControl('',[Validators.required, Validators.maxLength(39)]),
        subTitle : new FormControl('',[Validators.required, Validators.maxLength(349)]),
        image : new FormControl('',[Validators.required]),
      });

    }
    else{
      let text = this.main.deCodeNewsText(news.news_text.data);
      this.myGroup = this.formBuilder.group({
        htmlContent1 : new FormControl(text,[Validators.required, Validators.minLength(450)]),
      });

      this.newsMainInfoForm = this.formBuilder.group({
        title : new FormControl(news.news_title,[Validators.required, Validators.maxLength(39)]),
        subTitle : new FormControl(news.subtitle,[Validators.required, Validators.maxLength(349)]),
        image : new FormControl('',[Validators.required]),
      });
    }


    setTimeout(() =>$("#actionButton").click(), 500)
  }

  /**
   * If the file is not a jpeg, png, or jpg, then reset the form and set the validators to required.
   * @param {any} event - any
   * @returns The file is being returned.
   */
  onPhotoSelected(event : any) : void {
    if(event.target.files && event.target.files[0]){
      let file = event.target.files[0];
      if(file.type != "image/jpeg" && file.type != "image/png" && file.type != "image/jpg"){
        this.newsMainInfoForm.controls["image"].reset();
        this.newsMainInfoForm.controls["image"].setValidators([Validators.required]);
        this.newsMainInfoForm.get('image')?.updateValueAndValidity();
        this.main.toastr.info("The image only can be png, jpeg, or jpg");
        return;
      }
      

      this.file = <File> event.target.files[0];
    }
  }



  /**
   * If the checkValues() function returns false, then the toastr.info() function is called. 
   * If the checkValues() function returns true, then the disableButton variable is set to false.
   * @returns the value of the function checkValues()
   */
  saveMainInfo(){
    if(!this.checkValues()){
      this.main.toastr.info("If you do not fill in the main fields, you will not be able to publish the news!");
      return;
    }
    this.disableButton = false;
  } 

  /**
   * If the title, subtitle, and image are not empty, return true. Otherwise, return false.
   * @returns a boolean value.
   */
  checkValues() {
    if(this.newsMainInfoForm.value.title != "" && 
      this.newsMainInfoForm.value.subTitle != "" &&
      this.newsMainInfoForm.value.image != ""){
      return true;
    }
    return false;
  }

  /**
   * I send the data to the server, if the data is successfully sent, I redirect the user to the
   * profile page.
   */
  async submit() {
    this.disableButton = true;
    if(!this.checkValues()) {
      this.main.toastr.info("If you do not fill in the main fields, you will not be able to publish the news!");
      this.disableButton = true;
      return;
    }

    if(!this.myGroup.valid){
      this.main.toastr.error("New min length must be 450 characters.");
      this.disableButton = false;
      return;
    }

    let sessions = this.main.getCurrentUser();
    let photo = await this.main.provider.sendMainInfo(this.file);

    let news : any = {
      userId : sessions.currentUser.id,
      title : this.newsMainInfoForm.value.title, 
      subTitle :this.newsMainInfoForm.value.subTitle, 
      image : photo.message,
      newsText : this.myGroup.value.htmlContent1, 
      tags : null
    }
    if(this.main.getParamsNews() == undefined){
     let data = await this.main.provider.addNew(news);
     if(!data.status){
      this.main.toastr.error(data.message);
      this.disableButton = false;
      return;
      }
    }
    else{
      news['id'] = this.editNews.id;
      let data = await this.main.provider.editNew(news);
      if(!data.status){
        this.main.toastr.error(data.message);
        this.disableButton = false;
        return;
      }
      this.main.deleteParamsNews();
    }


    this.main.redirectTo('profile');
  }


}
