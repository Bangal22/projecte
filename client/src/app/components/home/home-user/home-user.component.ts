import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MainService } from '../../../services/main.service';
import * as $ from "jquery";

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.scss']
})
export class HomeUserComponent implements OnInit {
  @ViewChild('likeButton') likeButton! : ElementRef;
  @ViewChild('saveButton') saveButton! : ElementRef;

  public text! : any; 
  public lastId : number = -1;
  public news: any = [];
  public apiNewsNoRead : any;
  public apiNewsRead : any;
  public currentUserId: any = this.main.getCurrentUser().currentUser.id;
  public loading : boolean = false;

  constructor(public main:MainService) { }

/**
 * It loads the next X news items from the database and then scrolls to the top of the page.
 */
  async ngOnInit()  {
   this.loadedNextXNews();
   this.main.goUp();
 }



  /**
   * If the user has scrolled to the bottom of the page, load the next X news articles.
   */
  onScroll(){
    this.loadedNextXNews();
  }

/**
 * It loads the next X news from the server.
 * @returns {
 *     status: 1,
 *     content: [
 *       {
 *         id: 1,
 *         title: "title",
 *         description: "description",
 *         image: "image",
 *         date: "date",
 *         read: false
 *       },
 *       {
 *         id: 2
 */
  async loadedNextXNews() {
    this.loading = true;
    let id = 0;
    if(this.news.length != 0) id = this.news.length ;
    if(this.lastId == id){this.loading = false; return;} 
    this.lastId = id;

    let data = await this.main.provider.loadNextByNews(id);
    if(data.status == 1) this.news = this.news.concat(data.content);
    else if (data.status == 2){
      this.apiNewsNoRead = data.mediastackAPI;
      this.apiNewsRead = data.newsAPI;
    } 
    else{ this.main.toastr.warning(data.message); return;} 
    this.loading = false;
  }

 /**
  * It takes an id, and redirects the user to the news page with the id in the url.
  * @param {number} id - the id of the news item
  */
  goToNews(id:number){
    this.main.redirectTo(`news/${id}`);
  }

 /**
  * It takes the id of the news item and creates a url with it. Then it copies the url to the clipboard
  * @param {number} id - the id of the news item
  */
  share(id:number) {
    let url = `http://localhost:4200/news/${id}`
    navigator.clipboard.writeText(url);  
    this.main.toastr.success("URL copied to the clipboard");  
  }

 /**
  * The function takes a news object as a parameter and opens the url in a new window.
  * @param {any} news - any
  */
  goToApiNewsRead(news:any){
    window.open(news.url,"");

  }

 /**
  * It takes a news object as a parameter and opens a new window with the url of the news object.
  * @param {any} news - any
  */
  goToApiNewsNoRead(news:any){
    window.open(news.url,"_blank");

  }
}
