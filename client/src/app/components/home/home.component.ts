import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MainService } from '../../services/main.service';
import * as $ from "jquery";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 
  public privileges! : number;
  constructor(public main:MainService) { }

  /**
   * It gets the current user's role from the main service and assigns it to the privileges variable.
   */
  ngOnInit()  {
    this.privileges = this.main.getCurrentUser().currentUser.role;
  }




}
