import { Component, OnInit, EventEmitter,Output  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MainService } from '../../services/main.service';
import { NavbarComponent } from '../navbar/navbar.component';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() public updateNavBar = new EventEmitter<boolean>();

  public loginForm! : FormGroup;
  public disabledButton : boolean = false;

  constructor(
    public main:MainService,
    public navBarStatus : NavbarComponent,
    private formBuilder : FormBuilder,
    public auth: AngularFireAuth
    ) {}

 /**
  * The initForm() function is called when the component is initialized, and it returns a new FormGroup
  * object that contains two FormControl objects, one for the username and one for the password.
  */
  ngOnInit(): void {
    this.loginForm = this.initForm();
  }

  /**
   * When the user clicks the 'Forgot Password' link, redirect them to the 'recover-account' page.
   */
  recoverPassword() { 
    this.main.redirectTo('recover-account');
  }

/**
 * The function is called when the user clicks the submit button. It checks if the form is valid, if
 * not, it displays a warning message. If the form is valid, it calls the logUser function in the
 * authService. If the logUser function returns a status of false, it displays a warning message. If
 * the logUser function returns a status of true, it checks if the user_status is true. If it is, it
 * calls the setParamsErrorUser function and redirects to the error-user page. If the user_status is
 * false, it sets the token and currentUser in sessionStorage and reloads the page.
 */
  async submit(){
    this.disabledButton = true;
    if(!this.loginForm.valid){
      this.main.toastr.warning("Please, enter all required");
      this.disabledButton = false;
      return;
    }
    
    let data = await this.main.authService.logUser(this.loginForm.value);
    if(!data.status){
      this.main.toastr.warning(data.message);
      this.disabledButton = false;
      return;
    }

    if(data.user.user_status){
      this.main.setParamsErrorUser(data.user);
      this.main.redirectTo('error-user');
      return;
    }

    sessionStorage.setItem('token', data.token);
    sessionStorage.setItem('currentUser', JSON.stringify(data.user));
    window.location.reload();
  
  }

/**
 * The function returns a form group with two controls, nickname and password. The nickname control has
 * two validators, required and pattern. The password control has one validator, required.
 * @returns The formBuilder.group() method returns a FormGroup object.
 */
  initForm() : any{
    return this.formBuilder.group({
      nickname: ['',[Validators.required, Validators.pattern(/^(?=.*[a-z])[a-z\d_]{1,}$/) ] ],
      password : ['',[ Validators.required] ],
    });
  }


/**
 * It logs in a user with Google, then checks if the user exists in the database, if not, it creates a
 * new user, if it does, it logs the user in.
 * @returns The user object.
 */
  async loginGoogle(){
    let popUp = await this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    if(!popUp) return;

    let email = popUp.user?.email;
    let data = await this.main.provider.loginGoogle(email || 'undefined');

    if(!data.status){
      this.main.toastr.warning(data.message);
      return;
    }

    if(data.user.user_status){
      this.main.redirectTo('error404')
      return;
    }

    sessionStorage.setItem('token', data.token);
    sessionStorage.setItem('currentUser', JSON.stringify(data.user));
    window.location.reload();
  }

}
