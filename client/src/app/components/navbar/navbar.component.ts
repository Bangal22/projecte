import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  
  public navBarStatus : boolean = !!sessionStorage.getItem('token');
  public getUser : any;
  public userPhoto : any;
  public anonimPhoto : string = "assets/images/img-anonima.jpg";
  public notification : any = this.main.getParams();
  public currentUserId: any = this.main.getCurrentUser().currentUser.id;

  constructor(public main:MainService) { }

  /**
   * If the user has a photo, use it, otherwise use the default photo.
   */
  ngOnInit(): void {
    this.getUser = JSON.parse(sessionStorage.getItem('currentUser') || '{}');
    this.userPhoto = this.getUser.photo ? this.getUser.photo : this.anonimPhoto;
  }


 /**
  * The function is called when the user clicks the sign out button. It calls the signOut() function in
  * the authService.ts file, which removes the token from sessionStorage. It then sets the navBarStatus
  * to false, which hides the sign out button. Finally, it redirects the user to the home page.
  */
  signOut() {
    this.main.authService.signOut();
    this.navBarStatus = !!sessionStorage.getItem('token');
    this.main.redirectTo('home');
  }

 /**
  * If the user is logged in, then mark all notifications as viewed.
  * </code>
  */
  async notificationsViewed (){
    if(this.main.getParams()) await this.main.provider.notificationsViewed(this.currentUserId);
  }


}
