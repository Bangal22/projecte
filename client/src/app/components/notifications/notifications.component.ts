import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  public currentUserId: any = this.main.getCurrentUser().currentUser.id;
  public notifications! : any ;

  constructor(public main:MainService) { }

  /**
   * Every 10 seconds, call the getNotifications() function.
   */
  ngOnInit(): void {
    this.getNotifications();
    setInterval(()=>{this.getNotifications()},10000);
  }

  /**
   * I'm trying to get the notifications from the database and then check if there's any unread
   * notifications. If there is, I want to set the params to true, else false.
   */
  async getNotifications (){
    let data = await this.main.provider.getNotificationsById(this.currentUserId);
    this.notifications = data.result;
    let read = this.notifications.find((element:any) => !element.is_read);

    if(read != undefined) this.main.setParams(true);
    else this.main.setParams(false);
  }

  /**
   * It takes an id, and redirects to the news page with the id as a parameter.
   * @param {any} id - the id of the news item
   */
  goToNews(id:any){
    this.main.redirectTo(`news/${id}`);
  }


}
