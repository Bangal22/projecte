import { Component, OnInit ,Input} from '@angular/core';
import { MainService } from '../../../services/main.service';

@Component({
  selector: 'app-admin-alert',
  templateUrl: './admin-alert.component.html',
  styleUrls: ['./admin-alert.component.scss']
})
export class AdminAlertComponent implements OnInit {

  @Input() user : any;
  @Input() action : any;


  constructor(public main:MainService) { }

  ngOnInit(): void {

  }
/**
 * It takes the user's status and changes it to the action that was passed in.
 */

  async submit(){
    let data : any = await this.main.provider.adminActions(this.user, this.action);
    if(data.status) this.user.user_status = this.action;
    if(this.action == 1) this.main.toastr.success("User disabled");
    else if(this.action == 0) this.main.toastr.success("User enabled"); 
    else this.main.toastr.success("Deleted successfully!");
  
  }

  

}
