import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public followers : any = {data : ''};
  public followings : any = {data : ''};
  public user : any = {user : ''};
  public newsUser! : any ;
  public loading :boolean = false;
  public showButtonFollow = false;
  public following = false;
  public about! : string;
  public diabledButton : boolean = false;
  public news! : any;
  public deleteId! : any;
  public editNew! : any;

  public privileges : number = this.main.getCurrentUser().currentUser.role;
  public status : number = this.main.getCurrentUser().currentUser.user_status;
  public action! : number;
  public ff! : any;
  
  constructor(
    public main:MainService,
    private route: ActivatedRoute, 
    ) { }

  /**
   * The function is called when the component is initialized. It calls the goUp() function in the main
   * service, which scrolls the page to the top. It then calls the getProfile() function, which gets
   * the user's profile information from the database
   */
  ngOnInit(): void {
    this.main.goUp();
    this.getProfile()
  }

  /**
   * I'm trying to get the user's profile information, and if the user is not found, I want to redirect
   * to the error404 page.
   */
  async getProfile() {
    let nickname = this.main.getCurrentUser().currentUser.nickname;

    let id = this.main.getCurrentUser().currentUser.id;
    if(this.route.snapshot.params['username']) nickname = this.route.snapshot.params['username'];

    if(nickname != this.main.getCurrentUser().currentUser.nickname) this.showButtonFollow = true;
    if(this.main.getCurrentUser().currentUser.nickname == undefined) this.showButtonFollow = false;


    this.user = await this.main.provider.getProfile(nickname);
    if(!this.user.status){
      this.main.redirectTo('error404')
      return;
    }

    if(this.user.user.user_status && this.main.getCurrentUser().currentUser.role != 1){ 
      this.main.setParamsErrorUser(this.user.user);
      this.main.redirectTo('/error-user'); 
      return; 
    }

    this.followers = await this.main.provider.getFollowers(this.user.user.id);
    this.followings = await this.main.provider.getFollowing(this.user.user.id);
    this.newsUser = await this.main.provider.getNewsByUserId(this.user.user.id);
    
    let isFollowing = await this.main.provider.isFollowing(id,this.user.user.id);
    this.following = isFollowing.data.total;

    if(!this.user.user.about_me) this.user.user.about_me = "Bio no yet";

    let data = await this.main.provider.getNewsByUserId(this.user.user.id);
    this.news = data.content;
    this.loading = true;

  }
  /**
   * When the user clicks on a news item, the function will redirect the user to the news page with the
   * id of the news item.
   * @param {number} id - the id of the news item
   */
   goToNews(id:number){
    this.main.redirectTo(`news/${id}`);
  }

/**
 * It's a function that is called when a user clicks a button to follow another user. 
 * 
 * The function is called "follow" and it's a member of a class called "UserPage". 
 * 
 * The function is asynchronous. 
 * 
 * The function sets the value of a member variable called "diabledButton" to true. 
 * 
 * The function sets the value of a member variable called "following" to true. 
 * 
 * The function gets the id of the current user. 
 * 
 * The function calls a function called "follow" that is a member of a class called "Provider". 
 * 
 * The function passes the id of the current user and the id of the user that is being followed to the
 * "follow" function. 
 * 
 * The function sets the value of a member variable called "followers" to the value of the "total
 */
  async follow(){
    this.diabledButton = true;
    this.following = true;
    let id = this.main.getCurrentUser().currentUser.id;
    let data = await this.main.provider.follow(id,this.user.user.id);
    if(data.status)this.followers.total += -0+1; 
    this.diabledButton = false;
  }

  /**
   * It's a function that is called when a user clicks on a button to unfollow another user. 
   * 
   * The function is supposed to disable the button, change the value of the variable following to
   * false, and then call the function unFollow() from the provider. 
   * 
   * The function unFollow() from the provider is supposed to send a request to the server to unfollow
   * the user. 
   * 
   * If the request is successful, the function is supposed to decrease the value of the variable
   * followers.total by 1. 
   * 
   * The function is supposed to enable the button again. 
   * 
   * The problem is that the function doesn't work. 
   * 
   * The button is disabled, but the value of the variable following doesn't change to false, and the
   * value of the variable followers.total doesn't decrease by 1. 
   * 
   * The button is enabled again.
   */
  async unFollow(){
    this.diabledButton = true;
    this.following = false;
    let id = this.main.getCurrentUser().currentUser.id;
    let data = await this.main.provider.unFollow(id,this.user.user.id);
    if(data.status) this.followers.total -= 1;
    this.diabledButton = false;
  }

  /**
   * It takes a news object and sets it as a parameter in the main service, then redirects to the
   * create-new page.
   * @param {any} news - any
   */
  edit(news:any){

    this.main.setParamsNews(news);
    this.main.redirectTo('create-new');

  }

 /**
  * It deletes a news item from the database and hides it from the user.
  * @param {any} id - the id of the news
  * @returns The return value is the value of the last expression in the function.
  */
  async delete(id : any){
    let data = await this.main.provider.deleteNews(id);
    if(data.status){
      document.getElementById(id)!.style.display = "none";
      this.main.toastr.success("Deleted successfully!");
      return;
    }
    this.main.toastr.warning("An error occurred"); 
  }

  /**
   * "This function takes a string as a parameter and redirects the user to the profile page of the
   * user whose username is passed as a parameter."
   * </code>
   * @param {string} user - the user's name
   */
  goToProfile(user:string){
    location.href = `${environment.urlWeb}/#/${user}`;
    window.location.reload();
  }



}
