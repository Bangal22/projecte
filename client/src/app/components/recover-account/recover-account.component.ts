import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-recover-account',
  templateUrl: './recover-account.component.html',
  styleUrls: ['./recover-account.component.scss']
})
export class RecoverAccountComponent implements OnInit {
    
  public recoverAccountForm! : FormGroup;
  public messageEmail : boolean = false;

  constructor(public main : MainService, private formBuilder : FormBuilder,) { }

  /**
   * The function is called when the component is initialized. It creates a new form group and assigns
   * it to the recoverAccountForm variable.
   */
  ngOnInit(): void {
    this.recoverAccountForm = this.initForm();
  }


 /**
  * It returns a form group with two form controls, nickname and email. The nickname form control is
  * required, and the email form control is required and must be a valid email.
  * @returns A FormGroup object.
  */
  initForm(){
    return this.formBuilder.group({
      nickname: ['',[Validators.required ] ],
      email: ['',[ Validators.required, Validators.email] ],
    }) 
  }

  /**
   * It takes the form data, sends it to the provider, and if the provider returns a status of false,
   * it displays a toastr message.
   * @returns The data is being returned from the provider.
   */
  async submit() {
    let data = await this.main.provider.getUserByNicknameEmail(this.recoverAccountForm.value)
    if(!data.status){
      this.main.toastr.warning("The nickname or email are incorrect");
      return;
    }

    this.messageEmail = true;
  }

}
