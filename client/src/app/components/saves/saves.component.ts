import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-saves',
  templateUrl: './saves.component.html',
  styleUrls: ['./saves.component.scss']
})
export class SavesComponent implements OnInit {

  public news! : any;
  public userId : any = this.main.getCurrentUser().currentUser.id;

  constructor(public main:MainService) { }

 /**
  * It gets the saved news from the database and assigns it to the news variable.
  */
  async ngOnInit()  {

    let news = await this.main.provider.getSavesNews(this.userId);
    this.news = news.data;
    
  }


  /**
   * This function takes an id as a parameter and redirects the user to the news page with the id as a
   * parameter.
   * @param {any} id - the id of the news item
   */
  goToNews(id : any){
    this.main.redirectTo(`news/${id}`);
  }

}
