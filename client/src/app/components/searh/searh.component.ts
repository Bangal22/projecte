import { Component, OnInit, NgModule, ViewChild  } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-searh',
  templateUrl: './searh.component.html',
  styleUrls: ['./searh.component.scss']
})
export class SearhComponent implements OnInit {
  @ViewChild('inputSearch') inputSearch : any;

  public search! : string; 
  public channels : any = {status : null , result : []};
  public loading = true;
  public lastId : number = -1;
  public idCategoria = 1;
  
  constructor(public main:MainService) { }

  /**
   * The ngOnInit() function is a lifecycle hook that is called after Angular has initialized all
   * data-bound properties of a directive.
   */
  ngOnInit(): void {
    this.getChannels();
  }


  /**
   * It gets the channels from the server and adds them to the list of channels.
   */
  onScroll(){
    this.getChannels();
  }


  /**
   * "If the length of the channels.result array is not equal to zero, then set the id variable to the
   * length of the channels.result array. If the lastId variable is equal to the id variable, then
   * return. Otherwise, set the lastId variable to the id variable. Then, set the data variable to the
   * result of the getUserByTag function. Then, set the channels.result array to the concatenation of
   * the channels.result array and the data.result array. Finally, set the loading variable to false."
   * 
   * I hope this helps!</code>
   * @returns The data is being returned as an object with a property called result.
   */
  async getChannels() {
    var id = 0;

    if(this.channels.result?.length != 0) id = this.channels.result?.length ;
    if(this.lastId == id) return
    this.lastId = id;

    let data = await this.main.provider.getUserByTag(this.idCategoria, id);
    this.channels.result = this.channels.result.concat(data.result);
    this.loading = false;
  }

  /**
   * If the user does not exist, then the user is returned to the previous selection.
   * @returns The user is returned.
   */
  async submit(){
    let saveSelection = this.channels;
    this.loading = true;
    this.channels = await this.main.provider.getUsersSearch(this.search);
    if(!this.channels.status || this.channels.result.length == 0) {
      this.main.toastr.warning("This user does not exist.");
      this.channels = saveSelection; 
      this.loading = false;
      return;
    } 
    this.loading = false;
    this.idCategoria = -1;
  } 

  /**
   * It takes a string as an argument and redirects to the profile page of the user with the nickname
   * that was passed in
   * @param {string} nickname - the nickname of the user you want to go to
   */
  goToProfile(nickname : string){
    this.main.redirectTo(nickname);
  }
/**
 * It's a function that takes an id as a parameter and then it does some stuff.
 * @param {number} id - number
 */
  async category(id:number){
    this.loading = false;
    //this.inputSearch.nativeElement.focus()
    document.querySelectorAll('.category').forEach((element)=> element.classList.remove('active'))
    document.getElementById(`${id}`)!.classList.add('active');
    this.idCategoria = id;
    this.channels = await this.main.provider.getUserByTag(this.idCategoria, 0);

    this.loading = false;
  }

}
