import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { ImageCroppedEvent  } from 'ngx-image-cropper';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { environment } from '../../../environments/environment';
import * as $ from "jquery";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  public infoUsers! : any;
  public user : any = {user : {name : 'loading...', nickname : 'loading...', about_me : 'loading...', photo : 'loading...'}}; 

  public imageChangedEvent: any = '';
  public croppedImage: any = '';
  public urlImage: any = null;

  public hiddenImgView = true;
  public photoChanged = false;

  public settingForm! : FormGroup;
  public disabledButton = false;

  public changePasswordForm! : FormGroup;
  public diabledButtonP = true;

  public changeImg! : FormGroup;
  public fileControlName! : string;

  constructor(public main:MainService, private formBuilder : FormBuilder,) { }

  /**
   * I'm going to get the user's id, then I'm going to get the user's data, then I'm going to
   * initialize the form, then I'm going to initialize the form, then I'm going to initialize the form.
   */
  async ngOnInit() {
    
    let id = this.main.getCurrentUser().currentUser.id;
    this.settingForm = this.initForm();
    this.changePasswordForm = this.initFormChangePassword();
    this.changeImg = this.initChangePhotoProfile();
    this.user = await this.main.provider.getUserById(id); 
    this.settingForm = this.initForm();
    this.changePasswordForm = this.initFormChangePassword();
    this.changeImg = this.initChangePhotoProfile();
  }

 /**
  * It returns a form group with a single control named fileControlName. The control is required.
  * @returns A FormGroup
  */
  initChangePhotoProfile(){
    return this.formBuilder.group({
      fileControlName : ['', [Validators.required]]
    })
  }
  /**
   * It creates a form group with 4 form controls.
   * @returns The formBuilder.group() method returns a FormGroup object.
   */
  initForm() {
    return this.formBuilder.group({
      nickname : [`${this.user.user.nickname}`,[Validators.required, Validators.maxLength(19), Validators.pattern(/^(?=.*[a-z])[a-z\d_]{2,}$/)]],
      name : [`${this.user.user.name}`,[Validators.required, Validators.maxLength(19), Validators.pattern(/^(?=.*[a-z])[A-Za-z\d_\s]{2,}$/)]],
      about_me : [`${this.user.user.about_me}`,[Validators.maxLength(299)]],
      category : [`${this.user.user.tag_id}`]
    })
  }

 /**
  * The function returns a form group with three controls: currentPassword, newPassword, and
  * confirmNewPassword. The newPassword and confirmNewPassword controls have a pattern validator that
  * requires the value to be at least 8 characters long and contain at least one uppercase letter, one
  * lowercase letter, and one number.
  * </code>
  * @returns A FormGroup object.
  */
  initFormChangePassword(){
    return this.formBuilder.group({
      currentPassword : ['', [Validators.required]], 
      newPassword : ['', [Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)]],
      confirmNewPassword : ['', [Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)]]
    })
  }

/**
 * It takes the form data, sends it to the server, and if the server returns a success message, it
 * closes the modal.
 * @returns The data is being returned as an object.
 */
  async changePasswordSubmit(){
    if(!this.changePasswordForm.valid){
      this.main.toastr.warning("Fill in the fields correctly");
      return;
    }
    let passwords = {
      id : this.main.getCurrentUser().currentUser.id,
      passwords : this.changePasswordForm.value,
    }
    let data = await this.main.provider.updatePassword(passwords);
    if(!data.status){
      this.main.toastr.warning(data.message);
      return;
    }
    this.main.toastr.success(data.message);
    $('#close').click();

  }

/**
 * It checks if the file is a png, jpeg, or jpg. If it is, it returns true. If it isn't, it returns
 * false.
 * @param {any} event - any - the event that is triggered when the user selects a file
 * @returns a boolean value.
 */

  onPhotoSelected(event : any)  {
    if(event.target.files && event.target.files[0]){
      let file = event.target.files[0];
      if(file.type != "image/jpeg" && file.type != "image/png" && file.type != "image/jpg"){
        this.changeImg.controls["fileControlName"].reset();
        this.changeImg.controls["fileControlName"].setValidators([Validators.required]);
        this.changeImg.get('fileControlName')?.updateValueAndValidity();
        this.main.toastr.info("The image only can be png, jpeg, or jpg");
        return false;
      }
      return true;
    }
    return false;
  }


  async submit() {
    this.settingForm.markAllAsTouched();
    this.disabledButton = true;
  
    if(!this.settingForm.valid){
      this.main.toastr.warning("Please, enter all required");
      this.disabledButton = false;
      return;
    }

    let urlPhoto;
    if(!this.photoChanged) urlPhoto = this.main.getCurrentUser().currentUser.photo;
    else {
      let data = await this.main.provider.sendMainInfo(this.urlImage);
      urlPhoto = `${environment.urlApi}/uploads/${data.message}`;
    } 

    if(this.settingForm.value.about_me == "null") this.settingForm.value.about_me = null;

    
    let changeProfile = {
      id : this.main.getCurrentUser().currentUser.id,
      nickname: this.settingForm.value.nickname, 
      name : this.settingForm.value.name, 
      about_me : this.settingForm.value.about_me, 
      tag_id : parseInt(this.settingForm.value.category),
      photo : urlPhoto, 
      email : this.main.getCurrentUser().currentUser.email,
      role : this.main.getCurrentUser().currentUser.role,
      oldNickname : this.main.getCurrentUser().currentUser.nickname, 
      user_status : this.main.getCurrentUser().currentUser.user_status,
    } 

    let data = await this.main.provider.updateProfile(changeProfile);
    if(!data.status){
      this.main.toastr.warning(data.message);
      this.disabledButton = false;
      return;
    }
    
    delete changeProfile.oldNickname;
    delete changeProfile.about_me;
    this.disabledButton = false;
    sessionStorage.setItem('currentUser',JSON.stringify(changeProfile));  
    location.href = `${environment.urlWeb}/#/profile`; 
  }


/**
 * If the user has changed the photo, then show a success message, otherwise show an info message.
 */
  savePhoto(){
    this.photoChanged = this.urlImage ? true : false;
    if(this.photoChanged) this.main.toastr.success("Photo changed successfully!");
    else this.main.toastr.info("Photo no changed.");
  }


/**
 * If the user selected a valid image, then set the imageChangedEvent to the event and set the
 * hiddenImgView to false.
 * @param {any} event - any - the event that is triggered when the user selects a file.
 */
  fileChangeEvent(event: any): void {
    if(this.onPhotoSelected(event)){
      this.imageChangedEvent = event;
      this.hiddenImgView = false;
    }
  }
  /**
   * It takes the cropped image and converts it to a file.
   * @param {ImageCroppedEvent} event - ImageCroppedEvent
   */
  imageCropped(event: ImageCroppedEvent) {
    let base64Image : any = event.base64
    this.croppedImage = base64Image;
    this.urlImage = this.base64ToFile(base64Image);
    this.urlImage = this.blobToFile(this.urlImage, 'changed.png');
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
 /**
  * It takes a base64 string and returns a Blob
  * @param {string} base64Image - The base64 image string.
  * @returns A Blob object.
  */
  base64ToFile(base64Image: string): Blob {
      const split = base64Image.split(',');
      const type = split[0].replace('data:', '').replace(';base64', '');
      const byteString = atob(split[1]);
      const ab = new ArrayBuffer(byteString.length);
      const ia = new Uint8Array(ab);
      for (let i = 0; i < byteString.length; i += 1) {
          ia[i] = byteString.charCodeAt(i);
      }
      return new Blob([ab], {type});
  }

  /**
   * It takes a blob and a file name and returns a file object
   * @param {Blob} theBlob - Blob - the blob you want to convert to a file
   * @param {string} fileName - The name of the file you want to save the blob as.
   * @returns A File object.
   */
  blobToFile(theBlob: Blob, fileName:string){
    var b: any = theBlob;

    b.lastModifiedDate = new Date();
    b.name = fileName;
    
    return new File([theBlob], fileName, { lastModified: new Date().getTime(), type: theBlob.type })
    // return <File>b;
  }
}
