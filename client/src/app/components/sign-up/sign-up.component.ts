import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MainService } from '../../services/main.service';
import { AppComponent } from 'src/app/app.component';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  public signForm! : FormGroup;
  public verificationCode = false;
  public disabledButton = false;
  public userInfo : any;

  constructor(
    private formBuilder : FormBuilder,
    public main : MainService,
/**
 * Every second, the function tempsCrono() is called and the variable tempsInterval is set to the value
 * of the function tempsCrono().
 */
    public auth: AngularFireAuth
    ) 
  { 

  }
/**
 * This function initializes the form with the default values and returns the form.
 */

  ngOnInit(): void {
    this.signForm = this.initForm();
  }

  /**
   * If the form is valid, disable the button, send the form data to the server, and if the server
   * returns a success message, show the verification code input.
   */
  async submit() {
    this.disabledButton = true;
    this.signForm.markAllAsTouched();

    let data = await this.main.authService.sendUser(this.signForm.value);
    this.disabledButton = false;
    if(!data.status){ this.main.toastr.warning(data.message); return; }
    this.verificationCode = true;
  }

  /**
   * It returns a form group with 5 form controls, each with their own validators.
   * @returns A FormGroup object.
   */
  initForm() : any{
    return this.formBuilder.group({
      nickname: ['',[Validators.required, Validators.minLength(2), Validators.maxLength(19), Validators.pattern(/^(?=.*[a-z])[a-z\d_]{2,}$/) ] ],
      name : ['',[Validators.required, Validators.minLength(2),Validators.maxLength(19),  Validators.pattern(/^(?=.*[a-z])[a-zA-Z_\s+]{2,}$/)] ],
      email : ['',[ Validators.required, Validators.email] ],
      password : ['',[ Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)] ],
      confirmPassword : ['',[ Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/)] ],
    });
  }


  /**
   * It's a function that allows the user to sign up with google, and if the user is already
   * registered, it will log in.
   * @returns The user object is being returned.
   */
  async googleSingUp(){
    let popUp = await this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    if(!popUp) return;

    let user : any = {
      nickname : popUp.user?.displayName?.split(" ")[0],
      name : popUp.user?.displayName,
      email : popUp.user?.email,
      photo : popUp.user?.photoURL
    }

    let data = await this.main.provider.signUpGoogle(user);
    if(!data.status){
      this.main.toastr.warning(data.message);
      return;
    }

    sessionStorage.setItem('token', data.token);
    sessionStorage.setItem('currentUser', JSON.stringify(data.user));
    window.location.reload();
  }
}
