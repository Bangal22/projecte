import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
// import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.scss']
})
export class StopwatchComponent implements OnInit {

  public tempsInterval!: any;
  public minutes! : string;
  public seconds! : string;


  public now! : number;
  public fiveMinutesLater : number = new Date().getTime()+301000;

  constructor(private app : MainService) {

  }

/**
 * Every second, the function tempsCrono() is called and the variable tempsInterval is set to the value
 * of the function tempsCrono().
 */
  ngOnInit(): void {

    this.tempsCrono();
    this.tempsInterval = setInterval(() => {
      this.tempsCrono();
    }, 1000);

  }

  /**
   * It's a function that calculates the time remaining for a countdown timer.
   */
  tempsCrono() {

    let time = this.calculateDate()
    this.minutes = `${time.minutes}`;
    this.seconds = `${time.seconds}`;

    this.seconds = ('0' + this.seconds).slice(-2);
    this.minutes = ('0' + this.minutes).slice(-2);

    if (`${this.minutes}:${this.seconds}` == "00:00") {
      clearInterval(this.tempsInterval);
      this.app.redirectTo('/home');
    }
  }

  /**
   * The function calculates the difference between the current time and the time five minutes from
   * now, and returns an object with the minutes and seconds remaining.
   * @returns An object with two properties, minutes and seconds.
   */
  calculateDate() {
    this.now = new Date().getTime();
    let t =  this.fiveMinutesLater - this.now  ;
    let minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((t % (1000 * 60)) / 1000);
    return {
      minutes,
      seconds
    };
   
  }

}
