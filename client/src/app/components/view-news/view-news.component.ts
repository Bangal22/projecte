import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainService } from '../../services/main.service';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-view-news',
  templateUrl: './view-news.component.html',
  styleUrls: ['./view-news.component.scss']
})
export class ViewNewsComponent implements OnInit {
  public news!: any;
  public user: any = { photo: '' };
  public followers: any = { data: '' };
  public isFollowing!: any;
  public currentUserId: any = this.main.getCurrentUser().currentUser.id;
  public liked: boolean = false;
  public saved: boolean = false;

  public sessionUser: any = this.main.getCurrentUser().currentUser;

  constructor(
    public main: MainService,
    public router: ActivatedRoute,
    private clipboard: Clipboard
  ) { }

  /**
   * It gets the news id from the url, gets the news data from the database, gets the user data from
   * the database, gets the followers of the user, checks if the current user is following the user,
   * checks if the current user liked the news, checks if the current user saved the news, and then
   * sets the values of the variables to the data from the database.
   * @returns The data is being returned as an object.
   */
  async ngOnInit() {
    this.main.goUp();

    let id: any = this.router.snapshot.paramMap.get('id');
    let dataNews = await this.main.provider.getNewsById(id);
    this.news = dataNews.content[0];

    if (!this.news) { this.main.redirectTo('error'); return }
    let text = this.deCodeNewsText(this.news.news_text.data);
    document.getElementById('news-text')!.innerHTML = text;

    let dataUser = await this.main.provider.getUserById(this.news.user_id);
    this.user = dataUser.user;
    this.followers = await this.main.provider.getFollowers(this.news.user_id);

    let isFollowing = await this.main.provider.isFollowing(this.currentUserId, this.news.user_id);
    let isLiked = await this.main.provider.isLiked(this.currentUserId, this.news.id);
    let isSaved = await this.main.provider.isSaved(this.currentUserId, this.news.id);

    this.isFollowing = isFollowing.data.total;
    this.liked = isLiked.data.total;
    this.saved = isSaved.data.total;

  }


/**
 * It sets the isFollowing variable to true, gets the current user's id, and then calls the follow
 * function in the provider.ts file.
 */
  async follow() {
    this.isFollowing = true;
    let id = this.main.getCurrentUser().currentUser.id;
    await this.main.provider.follow(id, this.user.id);
  }

  async unFollow() {
    this.isFollowing = false;
    let id = this.main.getCurrentUser().currentUser.id;
    await this.main.provider.unFollow(id, this.user.id);
  }

 /**
  * The function is called when the user clicks on the like button. The function then sets the liked
  * property to true and calls the like function in the provider.
  */
  async like() {
    this.liked = true;
    await this.main.provider.like(this.currentUserId, this.news.id, this.news.user_id);
  }
 /**
  * If the user has liked the post, then unlike it, otherwise like it.
  */
  async unlike() {
    this.liked = false;

    await this.main.provider.unlike(this.currentUserId, this.news.id, this.news.user_id);
  }

  /**
   * This function sets the saved property to false and then calls the unsave function in the provider.
   */
  async unsave() {
    this.saved = false;
    await this.main.provider.unsave(this.currentUserId, this.news.id);
  }
  /**
   * The function saves the news and then displays a toastr message.
   */
  async save() {
    this.saved = true;
    this.main.toastr.success("News saved");
    await this.main.provider.save(this.currentUserId, this.news.id);
  }

  /**
   * It copies the url of the current news to the clipboard and then shows a toastr message.
   */
  async share() {
    let url = `http://seccioauriaweb.es/#/news/${this.news.id}`
    this.clipboard.copy(url)
    this.main.toastr.success("URL copied to the clipboard");
  }


  /**
   * It takes a string of bytes, converts it to a Uint8Array, then decodes it to a string.
   * @param {any} text - The text to be decoded.
   * @returns A string of text.
   */
  deCodeNewsText(text: any) {
    let utf8decoder = new TextDecoder();
    let bytes = new Uint8Array(text)
    let news = (utf8decoder.decode(bytes));
    return news;
  }



}
