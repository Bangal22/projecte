import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { RequestsService } from './requests.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private request: RequestsService) { }

 /**
  * It takes an object, converts it to a string, and then sends it to the server.
  * @param {object} data - object - the data you want to send to the server
  * @returns The data that is being returned is the data that is being sent to the server.
  */
  async sendUser(data: object) {
    const body = JSON.stringify(data)
    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/check/user', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

 /**
  * It takes a string, converts it to JSON, and sends it to the server.
  * @param {string} code - string - the code to send to the server
  * @returns The promise is being returned.
  */
  async sendCode(code: string) {
    const body = JSON.stringify({ code: code });
    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/code', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

/**
 * This function returns a promise that resolves to the data returned by the request.post function.
 * @returns A promise that resolves to the data returned from the server.
 */
  async saveUser() {
    const body = JSON.stringify({ user: null });
    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/user', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }


  /**
   * It takes an object, stringifies it, then sends it to the server.
   * @param {object} user - object - the user object that you want to send to the server
   * @returns A promise that resolves to the data returned from the server.
   */
  async logUser(user: object) {
    const body = JSON.stringify(user);
    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/login', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It removes the token and currentUser from sessionStorage.
   */
  signOut(): void {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('currentUser');
  }


  /**
   * It checks if the user is logged in.
   * @returns A boolean value.
   */
  loggedIn(): boolean {
    return !!sessionStorage.getItem('token');
  }
}
