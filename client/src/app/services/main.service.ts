import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastsComponent } from '../components/toasts/toasts.component';
import { ToastrService } from 'ngx-toastr';
import { ProviderService } from '../services/provider.service';
import { AuthService } from '../services/auth.service';
import { RequestsService } from '../services/requests.service';
import * as $ from "jquery";

@Injectable({
  providedIn: 'root'
})
export class MainService {
  public title = 'client';
  public params!: any;
  public notifications!: any;
  public userError!: any;
  constructor(
    public router: Router,

    public toast: ToastsComponent,
    public toastr: ToastrService,
    public provider: ProviderService,
    public authService: AuthService,
    public request: RequestsService,

  ) {
  }


  /**
   * This function takes a string as a parameter and navigates to the route that matches the string.
   * @param {string} params - string -&gt; the route you want to redirect to
   */
  redirectTo(params: string) {
    this.router.navigate([`/${params}`]);
  }
  /**
   * This function takes in a parameter of type any and assigns it to the notifications property of the
   * class.
   * @param {any} params - {
   */
  setParams(params: any) {
    this.notifications = params;
  }
  /**
   * It returns the value of the notifications property of the current instance of the class.
   * @returns the value of the notifications property.
   */
  getParams() {
    let params = this.notifications;
    return params;
  }

  /**
   * This function sets the params variable to the value of the params parameter
   * @param {any} params - {
   */
  setParamsNews(params: any) { this.params = params; }
  /**
   * It returns the value of the params variable.
   * @returns The params object.
   */
  getParamsNews() {
    let params = this.params;
    return params;
  }
  /**
   * It deletes the params property from the current object.
   */
  deleteParamsNews() {
    delete this.params;
  }
  /**
   * It takes a Uint8Array and returns a string.
   * @param {any} text - any - the text to decode
   * @returns The news is being returned.
   */
  deCodeNewsText(text: any) {
    let utf8decoder = new TextDecoder();
    let bytes = new Uint8Array(text)
    let news = (utf8decoder.decode(bytes));
    return news;
  }
  /**
   * It returns an object with two properties: token and currentUser. 
   * 
   * The token property is a string that is the value of the token key in sessionStorage. 
   * 
   * The currentUser property is an object that is the value of the currentUser key in sessionStorage. 
   * 
   * If the currentUser key is not in sessionStorage, then the currentUser property is an empty object.
   * @returns An object with two properties: token and currentUser.
   */
  getCurrentUser() {
    let currentUser = {
      token: sessionStorage.getItem('token'),
      currentUser: JSON.parse(sessionStorage.getItem('currentUser') || '{}'),
    }
    return currentUser;
  }

  /**
   * It calculates the time difference between the current time and the time passed in the function.
   * @param {any} date - any - the date you want to calculate
   * @returns The time difference between the current time and the time the post was created.
   */
  calculateDate(date: any) {
    date = new Date(date);
    let miliseconds = new Date().getTime() - date.getTime();

    if (miliseconds < 0) return "just now";

    let time = miliseconds / (1000 * 60 * 60 * 24);
    if (time >= 1) return Math.floor(time) + "d";

    time = (miliseconds % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
    if (time < 24 && time > 1) return Math.floor(time) + "h";

    time = (miliseconds % (1000 * 60 * 60)) / (1000 * 60);
    if (time < 59 && time > 1) return Math.floor(time) + "m";

    time = (miliseconds % (1000 * 60)) / 1000;
    return Math.floor(time) + "s"
  }


  /**
   * If the user is not enabled, return false. Otherwise, return true.
   * @returns A boolean value.
   */
  userEnabled(): boolean {
    let currentUser: any = sessionStorage.getItem('currentUser');
    let session: any = JSON.parse(currentUser);
    if (session.user_status == 1 || session.user_status == 2) {
      return false;
    }
    return true;
  }

  /**
   * This function takes in a parameter of type any and sets the userError property to the value of the
   * parameter.
   * @param {any} params - {
   */
  setParamsErrorUser(params: any) {
    this.userError = params;
  }

  /**
   * It returns the value of the userError property.
   * @returns the value of the variable params.
   */
  getParamsErrorUser() {
    let params = this.userError;

    return params;
  }

  /**
   * This function deletes the userError property from the object that this function is called on.
   */
  deleteParamsErrorUser() {
    delete this.userError;
  }


  /**
   * When the user clicks on the button, the page scrolls to the top.
   */
  goUp() {
    $(document).ready(function () {

      $('.ir-arriba').click(function () {
        $('body, html').animate({
          scrollTop: '0px'
        }, 300);
      });

      $(window).scroll(function () {
        if (window.scrollY > 0) {
          $('.ir-arriba').slideDown(300);
        } else {
          $('.ir-arriba').slideUp(300);
        }
      });

    });
  }
}
