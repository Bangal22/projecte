import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import { RequestsService } from './requests.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}
@Injectable({
  providedIn: 'root'
})
export class ProviderService {


  constructor(private http: HttpClient, private request: RequestsService) { }


  /**
   * 
   * The client will then take the JavaScript object and convert it to a TypeScript object.
   * @param {any} news - any - this is the object that will be sent to the server.
   * @returns A promise.
   */
  async addNew(news: any) {

    const body = JSON.stringify(news);

    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/news', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });

  }

  /**
   * It takes a news object, converts it to a string, and then sends it to the server.
   * @param {any} news - any -&gt; this is the object that I'm sending to the server
   * @returns The data that is being returned is the data that is being sent to the server.
   */
  async editNew(news: any) {
    const body = JSON.stringify(news);

    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/news/edit', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function
   * @param {any} id - the id of the user
   * @returns The data that is being returned is an array of objects.
   */
  async getNewsByUserId(id: any) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/newsByUserId/${id}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It takes a file, creates a formData object, appends the file to the formData object, and then sends
   * the formData object to the server
   * @param {File} photo - File - the file that you want to upload
   * @returns The data that is being returned is the file that was uploaded.
   */
  async sendMainInfo(photo: File) {
    const fd = new FormData();
    fd.append('photo', photo);

    return new Promise<any>((resolve, reject) => {
      this.request.formData('/uploads', fd,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {string} username - The username of the user you want to get the profile of.
   * @returns A promise that resolves to the data or error.
   */
  async getProfile(username: string) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/profile/${username}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {string} id - the id of the user
   * @returns The data is being returned as a promise.
   */
  async getUserById(id: string) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/settings/${id}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function
   * @param {string} username - The username of the user you want to get the profile of.
   * @returns A promise that resolves to the data or error.
   */
  async getUserProfile(username: string) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/${username}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {number} id - the id of the user you want to get the followers of
   * @returns A promise that resolves to an array of followers.
   */
  async getFollowers(id: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/followers/${id}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {number} id - number - the id of the user you want to get the followings of
   * @returns A promise that resolves to an array of objects.
   */
  async getFollowing(id: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/followings/${id}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * This function returns a promise that resolves to the data returned by the request.get function.
   * @param {any} userId - The user that is being followed
   * @param {any} followerId - The user who is following
   * @returns A promise that resolves to the data returned from the server.
   */
  async isFollowing(userId: any, followerId: any) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/isFollowing/${userId}/${followerId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It takes two numbers, sends them to the server, and returns a promise that resolves to the server's
   * response.
   * 
   * The function is async, so it returns a promise. 
   * 
   * The function takes two numbers, userId and followerId. 
   * 
   * The function sends those two numbers to the server. 
   * 
   * The function returns a promise that resolves to the server's response. 
   * 
   * The function is async, so it returns a promise. 
   * 
   * The function takes two numbers, userId and followerId. 
   * 
   * The function sends those two numbers to the server. 
   * 
   * The function returns a promise that resolves to the server's response. 
   * 
   * The function is async, so it returns a promise. 
   * 
   * The function takes two numbers, userId and followerId. 
   * 
   * The function sends
   * @param {number} userId - the id of the user you want to follow
   * @param {number} followerId - the user who is following
   * @returns A promise that resolves to the data returned from the server.
   */
  async follow(userId: number, followerId: number) {
    const body = JSON.stringify({ userId: userId, followerId: followerId });

    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/follow', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * This function is an asynchronous function that returns a promise that resolves to a data object or
   * an error object.
   * @param {number} userId - The userId of the user you want to unfollow
   * @param {number} followerId - The user who is following
   * @returns A promise that resolves to the data returned from the server.
   */
  async unFollow(userId: number, followerId: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.delete(`/delete/unFollow/${userId}/${followerId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It takes a user object, converts it to a string, and then sends it to the server.
   * @param {any} user - any - this is the object that is being passed to the function.
   * @returns The data that is being returned is the data that is being sent to the server.
   */
  async updateProfile(user: any) {
    const body = JSON.stringify(user);
    return new Promise<any>((resolve, reject) => {
      this.request.put(`/put/settings/update`, body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {number} lastId - the id of the last news item that was loaded
   * @returns A promise that resolves to an array of news objects.
   */
  async loadNextByNews(lastId: number) {


    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/getNextXNews/${lastId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });


  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {string} nickname - string
   * @returns a promise.
   */
  async getUsersSearch(nickname: string) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/search/${nickname}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {number} tagId - the id of the tag
   * @param {any} index - the index of the user you want to get
   * @returns a promise.
   */
  async getUserByTag(tagId: number, index: any) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/searchByTag/${tagId}/${index}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });

  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function
   * @param {number} id - The id of the news item you want to get.
   * @returns A promise that resolves to an object with the following properties:
   */
  async getNewsById(id: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/newsById/${id}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * This function is called when a user likes a post. It takes the userId, newsId, and newsUserId as
   * parameters and returns a promise.
   * @param {number} userId - The id of the user who liked the post
   * @param {number} newsId - the id of the news
   * @param {number} newsUserId - the user who posted the news
   * @returns A promise.
   */
  async like(userId: number, newsId: number, newsUserId: number) {
    const body = JSON.stringify({ userId: userId, newsId: newsId, newsUserId: newsUserId });

    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/like', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    });
  }

  /**
   * It deletes a like from the database and returns the data from the database.
   * @param {number} userId - the id of the user who is liking the news
   * @param {number} newsId - the id of the news
   * @param {number} newsUserId - the user who posted the news
   * @returns The data that is being returned is the data that is being sent from the server.
   */
  async unlike(userId: number, newsId: number, newsUserId: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.delete(`/delete/unlike/${userId}/${newsId}/${newsUserId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }
  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {any} userId - the id of the user who is logged in
   * @param {any} newsId - the id of the news
   * @returns A promise that resolves to an object that contains a boolean value.
   */

  async isLiked(userId: any, newsId: any) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/isLiked/${userId}/${newsId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It takes two parameters, userId and newsId, and sends them to the server
   * @param {number} userId - the id of the user who is saving the post
   * @param {number} newsId - the id of the news you want to save
   * @returns A promise that resolves to the data returned from the server.
   */
  async save(userId: number, newsId: number) {
    const body = JSON.stringify({ userId: userId, newsId: newsId });
    return new Promise<any>((resolve, reject) => {
      this.request.post('/post/save', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }
  /**
   * It's an async function that returns a promise that resolves to a data object or an error object
   * @param {number} userId - the id of the user
   * @param {number} newsId - the id of the news
   * @returns The data that is being returned is the data that is being saved in the database.
   */

  async unsave(userId: number, newsId: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.delete(`/delete/unsave/${userId}/${newsId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {any} userId - the id of the user
   * @param {any} newsId - the id of the news
   * @returns A promise that resolves to the data returned from the server.
   */
  async isSaved(userId: any, newsId: any) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/isSaved/${userId}/${newsId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }
  /**
   * It's a function that returns a promise that resolves to the result of a request.delete() call
   * @param {number} newsId - The id of the news you want to delete.
   * @returns The return type is a Promise.
   */

  async deleteNews(newsId: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.delete(`/delete/news/${newsId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {number} userId - number
   * @returns An array of objects.
   */
  async getSavesNews(userId: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/savesNews/${userId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * It takes an object, converts it to a string, and then sends it to the server.
   * @param {object} passwords - object
   * @returns A promise that resolves to the data returned from the server.
   */
  async updatePassword(passwords: object) {
    const body = JSON.stringify(passwords);
    return new Promise<any>((resolve, reject) => {
      this.request.put('/put/passwords', body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })

  }

  /**
   * It returns a promise that resolves to the data returned by the request.get function.
   * @param {number} userId - The id of the user who's notifications you want to get.
   * @returns A promise that resolves to an array of notifications.
   */
  async getNotificationsById(userId: number) {
    return new Promise<any>((resolve, reject) => {
      this.request.get(`/get/notifications/${userId}`,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });
    })
  }

  /**
   * This function takes a userId and returns a promise that resolves to the data returned from the
   * server or rejects with an error.
   * @param {number} userId - The id of the user who is viewing the notifications
   * @returns A promise.
   */
  async notificationsViewed(userId: number) {
    const body = JSON.stringify({ userId: userId });
    return new Promise<any>((resolve, reject) => {
      this.request.post("/post/notificationsViewed", body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });

    })
  }

  /**
   * It takes a user object, converts it to a JSON string, and sends it to the server.
   * @param {any} user - {
   * @returns A promise that resolves to the data returned from the server.
   */
  async signUpGoogle(user: any) {
    const body = JSON.stringify({ user: user });
    return new Promise<any>((resolve, reject) => {
      this.request.post("/post/signUpGoogle", body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });

    })
  }


  /**
   * It takes an email as a parameter, sends it to the server, and returns a promise that resolves to
   * the server's response.
   * @param {string} email - string
   * @returns The promise is being returned.
   */
  async loginGoogle(email: string) {
    const body = JSON.stringify({ email: email });
    return new Promise<any>((resolve, reject) => {
      this.request.post("/post/loginGoogle", body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });

    })

  }

  /**
   * It takes two parameters, user and status, and returns a promise that resolves to the data returned
   * from the server.
   * @param {number} user - the user id
   * @param {number} status - 0 =&gt; user is not admin
   * @returns The promise is being returned.
   */
  async adminActions(user: number, status: number) {
    const body = JSON.stringify({ user, status });
    return new Promise<any>((resolve, reject) => {
      this.request.post("/post/changeStatus", body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        });

    })
  }

  /**
   * It takes a user object, converts it to a string, and then sends it to the server
   * @param {any} user - any -&gt; this is the object that is being sent to the server.
   * @returns A promise that resolves to the data returned from the server.
   */
  async getUserByNicknameEmail(user: any) {
    const body = JSON.stringify(user);
    return new Promise<any>((resolve, reject) => {
      this.request.post("/get/user", body,
        (data: any) => {
          resolve(data);
        },
        (err: any) => {
          resolve(err);
        })
    })
  }
}


