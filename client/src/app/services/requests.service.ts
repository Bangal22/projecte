import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  //public server = "http://127.0.0.1:1000";
  public server = environment.urlApi;
  constructor(private http: HttpClient) { }

  /**
   * It takes a url, a body, a callback function and a callback error function as parameters and then it
   * makes a post request to the url with the body and if the request is successful it calls the
   * callback function with the data as a parameter and if the request is unsuccessful it calls the
   * callback error function with the error as a parameter.
   * @param {string} url - The url of the API endpoint you want to call.
   * @param {string} body - The data you want to send to the server.
   * @param {Function} callback - the function that will be called when the request is successful
   * @param {Function} callbackError - This is the function that will be called if the request fails.
   */
  public post(url: string, body: string, callback: Function, callbackError: Function) {

    this.http.post<any>(this.server + url, body, httpOptions).subscribe({
      next: (data: any) => {
        callback(data);
      },
      error: (error: any) => {
        callbackError(error);
      }
    });
  }


  /**
   * The function takes a url, a callback function and a callback error function. It then makes a get
   * request to the url and if the request is successful, it calls the callback function with the data
   * returned from the request. If the request is unsuccessful, it calls the callback error function with
   * the error returned from the request.
   * @param {string} url - The url to be called
   * @param {Function} callback - the function that will be called when the request is successful
   * @param {Function} callbackError - This is the function that will be called if the request fails.
   */
  public get(url: string, callback: Function, callbackError: Function) {

    this.http.get<any>(this.server + url, httpOptions).subscribe({
      next: (data: any) => {
        callback(data);
      },
      error: (error: any) => {
        callbackError(error);
      }
    });
  }


  /**
   * The function takes a url, a body, a callback function and a callback error function. It then uses
   * the http.put method to send a request to the server. If the request is successful, it calls the
   * callback function with the data returned from the server. If the request is unsuccessful, it calls
   * the callback error function with the error returned from the server.
   * @param {string} url - the url of the api
   * @param {string} body - The data you want to send to the server.
   * @param {Function} callback - the function that will be called when the request is successful
   * @param {Function} callbackError - Function
   */
  public put(url: string, body: string, callback: Function, callbackError: Function) {

    this.http.put<any>(this.server + url, body, httpOptions).subscribe({
      next: (data: any) => {
        callback(data);
      },
      error: (error: any) => {
        callbackError(error);
      }
    });
  }

  /**
   * This function takes a url, a callback function, and a callback error function, and then makes a
   * delete request to the url, and if the request is successful, it calls the callback function with
   * the data, and if the request is unsuccessful, it calls the callback error function with the error.
   * @param {string} url - The url of the API you want to call.
   * @param {Function} callback - the function that will be called when the request is successful
   * @param {Function} callbackError - This is the function that will be called if the request fails.
   */
  public delete(url: string, callback: Function, callbackError: Function) {

    this.http.delete<any>(this.server + url, httpOptions).subscribe({
      next: (data: any) => {
        callback(data);
      },
      error: (error: any) => {
        callbackError(error);
      }
    });
  }


  /**
   * It takes a url, a body, a callback function, and a callback error function. 
   * 
   * The body is a FormData object. 
   * 
   * The callback function is called when the request is successful. 
   * 
   * The callback error function is called when the request is unsuccessful. 
   * 
   * The function returns nothing.
   * @param {string} url - The url of the API you want to call
   * @param {FormData} body - FormData
   * @param {Function} callback - the function that will be called when the request is successful
   * @param {Function} callbackError - Function
   */
  public formData(url: string, body: FormData, callback: Function, callbackError: Function) {
    var httpOptionsFormData = {
      headers: new HttpHeaders({})
    }
    this.http.post<any>(this.server + url, body, httpOptionsFormData).subscribe({
      next: (data: any) => {
        callback(data);
      },
      error: (error: any) => {
        callbackError(error);
      }
    });
  }
}
