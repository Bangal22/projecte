"use strict";

var express = require('express');

var mysql = require('mysql');

var app = express();

var http = require('http');

var server = http.createServer(app);

var body_parser = require('body-parser');

var multer = require('multer');

var path = require('path');

var cors = require('cors');

var router = require('./routers/routers').router;

app.set('port', process.env.PORT || 1000);
app.use(express["static"](__dirname));
app.use(express.urlencoded({
  limit: '50mb',
  extended: false
}));
app.use(body_parser.json({
  limit: '50mb'
}));
app.use(cors());
app.use('/', router);
server.listen(app.get('port'), function () {
  console.log('listening on port ' + app.get('port'));
});