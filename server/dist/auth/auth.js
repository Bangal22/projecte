"use strict";

var jwt = require('jsonwebtoken');

var configToken = require('../config/auth');
/* The above code is a middleware function that is used to verify the token. */


exports.isAuthenticated = function (req, res, next) {
  if (!req.headers.authorization) return res.status(403).send({
    message: "You d'ont have a authorization"
  });
  var tocken = req.headers.authorization.split(' ')[0];
  if (tocken === 'null') return res.status(403).send({
    message: "You d'ont have a authorization"
  });
  var payload = jwt.verify(tocken, configToken.SECRET_TOKEN); // console.log(payload);

  next();
};