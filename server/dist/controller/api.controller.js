"use strict";

var url = "http://api.mediastack.com/v1/news?access_key=c43029240cf8ba4b23f41e55116d5d8a&countries=es"; //api newsapi.org -> 5841f640b34c442cbd407779db44e597

var http = require('http');

var https = require('node:https');
/* A function that returns a promise. */


exports.mediastack = function () {
  return new Promise(function (resolve, reject) {
    http.get(url, function (res) {
      var data = '';
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        data = JSON.parse(data);
        resolve(data);
      });
    }).on('error', function (err) {
      console.log(err.message);
    });
  });
};
/* A function that returns a promise. */


exports.newsapi = function () {
  var url = 'https://newsapi.org/v2/everything?' + 'q=Apple&' + 'from=2022-05-11&' + 'sortBy=popularity&' + 'apiKey=5841f640b34c442cbd407779db44e597';
  return new Promise(function (resolve, reject) {
    https.get(url, function (res) {
      var data = '';
      res.on('data', function (chunk) {
        data += chunk;
      });
      res.on('end', function () {
        data = JSON.parse(data);
        resolve(data);
      });
    }).on('error', function (err) {
      console.log(err.message);
    });
  });
};