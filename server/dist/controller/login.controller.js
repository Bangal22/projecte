"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var globalFunctions = require('../global/globalFunctions');

var modelUserInfo = require('../model/UserInfo.model');

var modelLogin = require('../model/login.model');

var jwt = require('jsonwebtoken');

var configToken = require('../config/auth');

var bcrypt = require("bcrypt");
/* The above code is a function that is used to log in. */


exports.addLogLogin = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var filter, user, compare, token;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            filter = checkUser(req);

            if (filter.status) {
              _context.next = 4;
              break;
            }

            res.send(filter);
            return _context.abrupt("return");

          case 4:
            _context.next = 6;
            return modelUserInfo.getUserByNikname(filter.params.nickname);

          case 6:
            user = _context.sent;

            if (user.status) {
              _context.next = 10;
              break;
            }

            res.send(user);
            return _context.abrupt("return");

          case 10:
            _context.next = 12;
            return bcrypt.compare(filter.params.password, user.user.password);

          case 12:
            compare = _context.sent;

            if (compare) {
              _context.next = 16;
              break;
            }

            res.send({
              status: 0,
              message: "Invalid nickname or password"
            });
            return _context.abrupt("return");

          case 16:
            token = jwt.sign({
              nickname: user.nickname
            }, configToken.SECRET_TOKEN);
            res.send({
              status: 1,
              message: 'Succesfuly login',
              token: token,
              user: {
                id: user.user.id,
                name: user.user.name,
                nickname: user.user.nickname,
                photo: user.user.photo,
                email: user.user.email,
                role: user.user.role_id,
                user_status: user.user.user_status
              }
            });

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/* A function that is used to log in using google. */


exports.loginGoogle = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var data, token;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return modelLogin.loginGoogle(req.body.email);

          case 2:
            data = _context2.sent;

            if (data.status) {
              _context2.next = 6;
              break;
            }

            res.send(data);
            return _context2.abrupt("return");

          case 6:
            token = jwt.sign({
              nickname: data.user.nickname
            }, configToken.SECRET_TOKEN);
            data['token'] = token;
            res.send(data);

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();
/**
 * It takes a request object, and returns an array of objects with a key of param, and a value of an
 * array of two strings.
 * @param req - is the request object
 * @returns [ { param: [ 'nickname', 'test' ] },
 *   { param: [ 'password', 'test' ] } ]
 */


function checkUser(req) {
  var params = [{
    param: ["nickname", req.body.nickname]
  }, {
    param: ["password", req.body.password]
  }];
  return globalFunctions.FILTER_SANITIZE_STRING(params);
}