"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var globalFunctions = require('../global/globalFunctions');

var modelNews = require('../model/news.model');

var jwt = require('jsonwebtoken');

var configToken = require('../config/auth');

var modelNotifications = require('../model/notifications.model');

var bcrypt = require("bcrypt");

var multer = require('multer');

var apiController = require('./api.controller');
/**API PETITIONS**/


var mediastackAPI;
var newsapiAPI;
(0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
  return _regenerator["default"].wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return apiController.mediastack();

        case 2:
          mediastackAPI = _context.sent;
          _context.next = 5;
          return apiController.newsapi();

        case 5:
          newsapiAPI = _context.sent;

        case 6:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}))();
/**API PETITIONS**/

/* A function that is being exported. */

exports.addNews = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var params, clearParams, data;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            params = [{
              param: ["title", req.body.title]
            }, {
              param: ["subtitle", req.body.subTitle]
            }];
            clearParams = globalFunctions.FILTER_SANITIZE_STRING(params);

            if (clearParams.status) {
              _context2.next = 5;
              break;
            }

            res.send(clearParams);
            return _context2.abrupt("return");

          case 5:
            req.body.title = clearParams.params.title;
            req.body.subtitle = clearParams.params.subtitle;

            if (!(req.body.newsText.length > 300000)) {
              _context2.next = 10;
              break;
            }

            res.send({
              staus: 0,
              message: "You have exceeded the allowed memory (250kb). We advise you not to use images larger than 430,000px or text no longer than 300,000 characters."
            });
            return _context2.abrupt("return");

          case 10:
            _context2.next = 12;
            return modelNews.addNews(req.body);

          case 12:
            data = _context2.sent;
            res.send(data);

          case 14:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x, _x2) {
    return _ref2.apply(this, arguments);
  };
}();
/* A function that is being exported. */


exports.editNews = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var params, clearParams, data;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            params = [{
              param: ["title", req.body.title]
            }, {
              param: ["subtitle", req.body.subTitle]
            }];
            clearParams = globalFunctions.FILTER_SANITIZE_STRING(params);

            if (clearParams.status) {
              _context3.next = 5;
              break;
            }

            res.send(clearParams);
            return _context3.abrupt("return");

          case 5:
            req.body.title = clearParams.params.title;
            req.body.subtitle = clearParams.params.subtitle;
            console.log(req.body.newsText.length);

            if (!(req.body.newsText.length > 300000)) {
              _context3.next = 11;
              break;
            }

            res.send({
              staus: 0,
              message: "You have exceeded the allowed memory (250kb). We advise you not to use images larger than 250kb or text no longer than 300,000 characters."
            });
            return _context3.abrupt("return");

          case 11:
            _context3.next = 13;
            return modelNews.editNews(req.body);

          case 13:
            data = _context3.sent;
            res.send(data);

          case 15:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x3, _x4) {
    return _ref3.apply(this, arguments);
  };
}();

exports.getNewsByUserId = /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return modelNews.getNewsByUserId(req.params.userId);

          case 2:
            data = _context4.sent;
            res.send(data);

          case 4:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x5, _x6) {
    return _ref4.apply(this, arguments);
  };
}();

exports.getNewsById = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return modelNews.getNewsById(req.params.id);

          case 2:
            data = _context5.sent;
            res.send(data);

          case 4:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x7, _x8) {
    return _ref5.apply(this, arguments);
  };
}();

exports.getNextXNews = /*#__PURE__*/function () {
  var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(req, res) {
    var id, data;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            id = parseInt(req.params.index);
            _context6.next = 3;
            return modelNews.getNextXNews(id);

          case 3:
            data = _context6.sent;
            if (data.content.length == 0) data = {
              status: 2,
              newsAPI: newsapiAPI.articles,
              mediastackAPI: mediastackAPI.data
            };
            res.send(data);

          case 6:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function (_x9, _x10) {
    return _ref6.apply(this, arguments);
  };
}();

exports.save = /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return modelNews.save(req.body.userId, req.body.newsId);

          case 2:
            data = _context7.sent;
            res.send(data);

          case 4:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x11, _x12) {
    return _ref7.apply(this, arguments);
  };
}();

exports.unsave = /*#__PURE__*/function () {
  var _ref8 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.next = 2;
            return modelNews.unsave(req.params.user_id, req.params.news_id);

          case 2:
            data = _context8.sent;
            res.send(data);

          case 4:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function (_x13, _x14) {
    return _ref8.apply(this, arguments);
  };
}();

exports.isSaved = /*#__PURE__*/function () {
  var _ref9 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return modelNews.isSaved(req.params.user_id, req.params.news_id);

          case 2:
            data = _context9.sent;
            res.send(data);

          case 4:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function (_x15, _x16) {
    return _ref9.apply(this, arguments);
  };
}();

exports.like = /*#__PURE__*/function () {
  var _ref10 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.next = 2;
            return modelNews.like(req.body.userId, req.body.newsId);

          case 2:
            data = _context10.sent;
            _context10.next = 5;
            return modelNotifications.setNewNotification(req.body.userId, req.body.newsUserId, 3, 0, req.body.newsId);

          case 5:
            res.send(data);

          case 6:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));

  return function (_x17, _x18) {
    return _ref10.apply(this, arguments);
  };
}();

exports.unlike = /*#__PURE__*/function () {
  var _ref11 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee11(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return modelNews.unlike(req.params.user_id, req.params.news_id);

          case 2:
            data = _context11.sent;
            _context11.next = 5;
            return modelNotifications.removeNotification(req.params.user_id, req.params.news_user_id, 3);

          case 5:
            res.send(data);

          case 6:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));

  return function (_x19, _x20) {
    return _ref11.apply(this, arguments);
  };
}();

exports.isLiked = /*#__PURE__*/function () {
  var _ref12 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee12(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.next = 2;
            return modelNews.isLiked(req.params.user_id, req.params.news_id);

          case 2:
            data = _context12.sent;
            res.send(data);

          case 4:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));

  return function (_x21, _x22) {
    return _ref12.apply(this, arguments);
  };
}();
/* Exporting a function. */


exports.deleteNewsById = /*#__PURE__*/function () {
  var _ref13 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee13(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.next = 2;
            return modelNews.deleteNewsById(req.params.id);

          case 2:
            data = _context13.sent;
            res.send(data);

          case 4:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  }));

  return function (_x23, _x24) {
    return _ref13.apply(this, arguments);
  };
}();
/* A function that is being exported. */


exports.getSavesNews = /*#__PURE__*/function () {
  var _ref14 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee14(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.next = 2;
            return modelNews.getSavesNews(req.params.user_id);

          case 2:
            data = _context14.sent;
            res.send(data);

          case 4:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14);
  }));

  return function (_x25, _x26) {
    return _ref14.apply(this, arguments);
  };
}();