"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

// const validator = require('validator');
var globalFunctions = require('../global/globalFunctions');

var modelSignUp = require('../model/signUp.model');

var modelUserInfo = require('../model/UserInfo.model');

var mail = require('../mail/mail');

var jwt = require('jsonwebtoken');

var configToken = require('../config/auth');

var bcrypt = require("bcrypt");

var modelNotifications = require('../model/notifications.model');

var cloudingIO = require('../config/clouding.io');

var user;
var code;
/* A function that is being exported. */

exports.registerConfigurations = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var params, data, validation;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            params = [{
              param: ["nickname", req.body.nickname]
            }, {
              param: ["name", req.body.name]
            }, {
              param: ["email", req.body.email]
            }, {
              param: ["password", req.body.password]
            }, {
              param: ["confirmPassword", req.body.confirmPassword]
            }];
            data = globalFunctions.FILTER_SANITIZE_STRING(params);

            if (data.status) {
              _context.next = 5;
              break;
            }

            res.send(data);
            return _context.abrupt("return");

          case 5:
            _context.next = 7;
            return modelUserInfo.getUserByNicknameEmail(data.params.nickname, data.params.email);

          case 7:
            validation = _context.sent;

            if (validation.status) {
              _context.next = 11;
              break;
            }

            res.send(validation);
            return _context.abrupt("return");

          case 11:
            if (!(data.params.password != data.params.confirmPassword)) {
              _context.next = 14;
              break;
            }

            res.send({
              status: 0,
              message: "No equals passwords"
            });
            return _context.abrupt("return");

          case 14:
            ;
            _context.next = 17;
            return bcrypt.hash(data.params.password, 10);

          case 17:
            data.params.password = _context.sent;
            code = globalFunctions.randomVerficateCode();
            user = data.params;
            user.photo = "";
            user.photo = "".concat(cloudingIO.config, "/uploads/default.jpg");
            mail.sendMail(data.params.email, "Code: ".concat(code));
            res.send(validation);

          case 24:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/* A function that is being exported. */


exports.checkCodeClient = function (req, res) {
  if (req.body.code == code) {
    res.send({
      status: 1,
      message: 'Valid Code'
    });
    return;
  }

  res.send({
    status: 0,
    message: 'Invalid Code'
  });
};
/* A function that is being exported. */


exports.addLogRegister = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var data, token;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return modelSignUp.addUser(user);

          case 2:
            data = _context2.sent;
            token = jwt.sign({
              nickname: user.nickname
            }, configToken.SECRET_TOKEN);
            user.id = data.lastId;
            res.send({
              status: 1,
              message: 'Succesfuly registry',
              token: token,
              user: {
                name: user.name,
                nickname: user.nickname,
                photo: user.photo,
                id: user.id,
                email: user.email,
                user_status: 0,
                role: 2
              }
            });

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();
/* A function that is being exported. */


exports.signUpGoogle = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var user, password, token, data;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            user = req.body.user;
            user.nickname = user.nickname.toLowerCase() + "_" + globalFunctions.randomName();
            password = globalFunctions.randomPassword();
            user['password'] = password;
            _context3.next = 6;
            return bcrypt.hash(user.password, 10);

          case 6:
            user.password = _context3.sent;
            user['user_status'] = 0;
            user['role'] = 2;
            token = jwt.sign({
              nickname: user.nickname
            }, configToken.SECRET_TOKEN);
            _context3.next = 12;
            return modelSignUp.signUpGoogle(user);

          case 12:
            data = _context3.sent;

            if (!data.status) {
              _context3.next = 22;
              break;
            }

            mail.sendMail(user.email, "Password: ".concat(password));
            _context3.next = 17;
            return modelNotifications.setNewNotification(38, data.lastId, 4, 0, null);

          case 17:
            user['id'] = data.lastId;
            data['token'] = token;
            data['user'] = user;
            res.send(data);
            return _context3.abrupt("return");

          case 22:
            res.send(data);

          case 23:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();