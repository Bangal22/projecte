"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

// const validator = require('validator');
var globalFunctions = require('../global/globalFunctions');

var modelUserInfo = require('../model/UserInfo.model');

var modelNotifications = require('../model/notifications.model');

var mail = require('../mail/mail');

var bcrypt = require("bcrypt");
/* This is a function that is exported to be used in another file. */


exports.getUserByNicknameEmail = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var data, code, bcryptCode;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return modelUserInfo.getUserByNE(req.body.nickname, req.body.email);

          case 2:
            data = _context.sent;

            if (!data.status) {
              _context.next = 11;
              break;
            }

            code = globalFunctions.randomPassword();
            _context.next = 7;
            return bcrypt.hash(code, 10);

          case 7:
            bcryptCode = _context.sent;
            _context.next = 10;
            return modelUserInfo.updatePassword(bcryptCode, data.result.id);

          case 10:
            mail.sendMail(req.body.email, "New password: ".concat(code));

          case 11:
            res.send(data);

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.getUserByNikname = /*#__PURE__*/function () {
  var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return modelUserInfo.getUserByNikname(req.params.username);

          case 2:
            data = _context2.sent;
            res.send(data);

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.getUserById = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return modelUserInfo.getUserById(req.params.id);

          case 2:
            data = _context3.sent;
            res.send(data);

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.getFollowers = /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return modelUserInfo.getFollowers(req.params.id);

          case 2:
            data = _context4.sent;
            res.send(data);

          case 4:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x7, _x8) {
    return _ref4.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.getFollowing = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return modelUserInfo.getFollowing(req.params.id);

          case 2:
            data = _context5.sent;
            // console.log(data)
            res.send(data);

          case 4:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.isFollowing = /*#__PURE__*/function () {
  var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.next = 2;
            return modelUserInfo.isFollowing(req.params.idCurrentUser, req.params.idUser);

          case 2:
            data = _context6.sent;
            res.send(data);

          case 4:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function (_x11, _x12) {
    return _ref6.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.follow = /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _context7.next = 2;
            return modelUserInfo.follow(req.body.userId, req.body.followerId);

          case 2:
            data = _context7.sent;
            _context7.next = 5;
            return modelNotifications.setNewNotification(req.body.userId, req.body.followerId, 1, 0, null);

          case 5:
            res.send(data);

          case 6:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x13, _x14) {
    return _ref7.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.unfollow = /*#__PURE__*/function () {
  var _ref8 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.next = 2;
            return modelUserInfo.unfollow(req.params.user_id, req.params.follower_id);

          case 2:
            data = _context8.sent;
            _context8.next = 5;
            return modelNotifications.removeNotification(req.params.user_id, req.params.follower_id, 1);

          case 5:
            res.send(data);

          case 6:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function (_x15, _x16) {
    return _ref8.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.updateUser = /*#__PURE__*/function () {
  var _ref9 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(req, res) {
    var nicknameExists, data;
    return _regenerator["default"].wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return modelUserInfo.userExists(req.body.nickname);

          case 2:
            nicknameExists = _context9.sent;

            if (!(nicknameExists.data.length > 0)) {
              _context9.next = 7;
              break;
            }

            if (!(nicknameExists.data[0].nickname != req.body.oldNickname)) {
              _context9.next = 7;
              break;
            }

            res.send({
              staus: 0,
              message: "The nickname already exist."
            });
            return _context9.abrupt("return");

          case 7:
            _context9.next = 9;
            return modelUserInfo.updateUser(req.body);

          case 9:
            data = _context9.sent;
            res.send(data);

          case 11:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function (_x17, _x18) {
    return _ref9.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.getUsersSearch = /*#__PURE__*/function () {
  var _ref10 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.next = 2;
            return modelUserInfo.getUsersSearch(req.params.nickname);

          case 2:
            data = _context10.sent;
            res.send(data);

          case 4:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));

  return function (_x19, _x20) {
    return _ref10.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.getUserByTag = /*#__PURE__*/function () {
  var _ref11 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee11(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return modelUserInfo.getUserByTag(req.params.tagId, req.params.index);

          case 2:
            data = _context11.sent;
            console.log(data); // console.log(data)

            res.send(data);

          case 5:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));

  return function (_x21, _x22) {
    return _ref11.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.updatePassword = /*#__PURE__*/function () {
  var _ref12 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee12(req, res) {
    var user, compare, password, data;
    return _regenerator["default"].wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.next = 2;
            return modelUserInfo.getUserById(req.body.id);

          case 2:
            user = _context12.sent;
            _context12.next = 5;
            return bcrypt.compare(req.body.passwords.currentPassword, user.user.password);

          case 5:
            compare = _context12.sent;

            if (compare) {
              _context12.next = 9;
              break;
            }

            res.send({
              status: 0,
              message: 'Current password is incorrect'
            });
            return _context12.abrupt("return");

          case 9:
            if (!(req.body.passwords.newPassword != req.body.passwords.confirmNewPassword)) {
              _context12.next = 12;
              break;
            }

            res.send({
              status: 0,
              message: 'Passwords are not the same'
            });
            return _context12.abrupt("return");

          case 12:
            _context12.next = 14;
            return bcrypt.hash(req.body.passwords.newPassword, 10);

          case 14:
            password = _context12.sent;
            _context12.next = 17;
            return modelUserInfo.updatePassword(password, req.body.id);

          case 17:
            data = _context12.sent;
            res.send({
              status: 1,
              message: 'Passwords changed successfully!'
            });

          case 19:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));

  return function (_x23, _x24) {
    return _ref12.apply(this, arguments);
  };
}();
/* A function that is exported to be used in another file. */


exports.changeStatus = /*#__PURE__*/function () {
  var _ref13 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee13(req, res) {
    var data;
    return _regenerator["default"].wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.next = 2;
            return modelUserInfo.changeStatus(req.body.user.id, req.body.status);

          case 2:
            data = _context13.sent;

            if (req.body.status == 2) {
              mail.sendMail(req.body.user.email, "We have deleted your account because we consider that you do not comply with the community rules.");
            }

            res.send(data);

          case 5:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  }));

  return function (_x25, _x26) {
    return _ref13.apply(this, arguments);
  };
}();