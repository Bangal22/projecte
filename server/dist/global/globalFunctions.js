"use strict";

function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var _require = require("qs"),
    parse = _require.parse;
/* A function that is used to sanitize the input data. */


exports.FILTER_SANITIZE_STRING = function (params) {
  var clearParams = {};

  var _iterator = _createForOfIteratorHelper(params),
      _step;

  try {
    for (_iterator.s(); !(_step = _iterator.n()).done;) {
      var key = _step.value;
      var param = key.param[1].replaceAll(/\x00|<[^>]*>?/g, '');
      param = param.replaceAll('\'', '&#39;');
      param = param.replaceAll('"', '&#34;');
      if (!param) return {
        status: 0,
        message: "".concat(key.param[0], " is empty")
      };
      clearParams[key.param[0]] = param;
    }
  } catch (err) {
    _iterator.e(err);
  } finally {
    _iterator.f();
  }

  return {
    status: 1,
    params: clearParams
  };
};
/* A function that is used to generate a random code. */


exports.randomVerficateCode = function () {
  var code = "";
  var pattern = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (var index = 0; index < 5; index++) {
    var randomIndex = Math.floor(Math.random() * (pattern.length - 1) + 0);
    code += pattern.charAt(randomIndex);
  }

  return code;
};
/* Generating a random name. */


exports.randomName = function () {
  var code = "";
  var pattern = "1234567890abcdefghijklmnopqrstuvwxyz";

  for (var index = 0; index < 5; index++) {
    var randomIndex = Math.floor(Math.random() * (pattern.length - 1) + 0);
    code += pattern.charAt(randomIndex);
  }

  return code;
};
/* A function that is used to get the current date and time. */


exports.getDateTypeSQL = function () {
  var ts = new Date();
  ts.setMinutes(ts.getMinutes() - ts.getTimezoneOffset());
  return ts.toISOString().slice(0, 19).replace('T', ' ');
};
/* Generating a random password. */


exports.randomPassword = function () {
  var code = "";
  var pattern = "1234567890abcdefghijklmnopqrstuvwxyz1234568790ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

  for (var index = 0; index < 20; index++) {
    var randomIndex = Math.floor(Math.random() * (pattern.length - 1) + 0);
    code += pattern.charAt(randomIndex);
  }

  return code;
};