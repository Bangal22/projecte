"use strict";

// const uuid = require('uuid');
var path = require('path');

var multer = require('multer');

var express = require('express');
/* Creating a storage object for multer to use. */


var storage = multer.diskStorage({
  destination: function destination(req, file, cb) {
    cb(null, __dirname + '/../uploads');
  },
  filename: function filename(req, file, cb) {
    var uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, "d" + uniqueSuffix + path.extname(file.originalname));
  }
});
var upload = multer({
  storage: storage
});
module.exports = upload;