"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var db = require('../config/db').connection;

var NEXT_X_USERS = 10;
/* A function that is being exported to be used in another file. */

exports.getUserByNicknameEmail = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(nickname, email) {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            return _context2.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT nickname, email FROM users WHERE (nickname = ? OR email = ?)";
              var value = [nickname, email];
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(err, result) {
                  return _regenerator["default"].wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          if (!err) {
                            _context.next = 5;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context.abrupt("return");

                        case 5:
                          if (!(result.length == 0)) {
                            _context.next = 8;
                            break;
                          }

                          resolve({
                            status: 1,
                            message: "Succesfuly"
                          });
                          return _context.abrupt("return");

                        case 8:
                          if (result[0].nickname == nickname) resolve({
                            status: 0,
                            message: "Nickname already exist"
                          });else resolve({
                            status: 0,
                            message: "Email already exist"
                          });

                        case 9:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x3, _x4) {
                  return _ref2.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context2.prev = 4;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 4]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.getUserByNE = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(nickname, email) {
    var sql, value;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            sql = "SELECT id, nickname, email FROM users WHERE nickname = ? AND email = ?";
            value = [nickname, email];
            return _context4.abrupt("return", new Promise(function (resolve, reject) {
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(err, result) {
                  return _regenerator["default"].wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          if (!err) {
                            _context3.next = 5;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context3.abrupt("return");

                        case 5:
                          if (!(result.length == 0)) {
                            _context3.next = 10;
                            break;
                          }

                          resolve({
                            status: 0,
                            message: "Nickname or Email not exisit."
                          });
                          return _context3.abrupt("return");

                        case 10:
                          resolve({
                            status: 1,
                            result: result[0],
                            message: "Succesfuly!"
                          });

                        case 11:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3);
                }));

                return function (_x7, _x8) {
                  return _ref4.apply(this, arguments);
                };
              }());
            }));

          case 6:
            _context4.prev = 6;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);

          case 9:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 6]]);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.getUserByNikname = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(nickname) {
    var sql, value;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            sql = "SELECT u.*, t.name as tag_name FROM users AS u " + "INNER JOIN tags AS t ON t.id = u.tag_id " + "" + "WHERE BINARY u.nickname = ? AND u.user_status != 2";
            value = [nickname];
            return _context6.abrupt("return", new Promise(function (resolve, reject) {
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(err, result) {
                  return _regenerator["default"].wrap(function _callee5$(_context5) {
                    while (1) {
                      switch (_context5.prev = _context5.next) {
                        case 0:
                          if (!err) {
                            _context5.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context5.abrupt("return");

                        case 4:
                          if (!(result.length == 0)) {
                            _context5.next = 7;
                            break;
                          }

                          resolve({
                            status: 0,
                            message: "Nickname or password incorrect"
                          });
                          return _context5.abrupt("return");

                        case 7:
                          resolve({
                            status: 1,
                            user: result[0]
                          });

                        case 8:
                        case "end":
                          return _context5.stop();
                      }
                    }
                  }, _callee5);
                }));

                return function (_x10, _x11) {
                  return _ref6.apply(this, arguments);
                };
              }());
            }));

          case 6:
            _context6.prev = 6;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);

          case 9:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 6]]);
  }));

  return function (_x9) {
    return _ref5.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.getUserById = /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(id) {
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            return _context8.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT * FROM users WHERE id = ?";
              var value = [id];
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref8 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(err, result) {
                  return _regenerator["default"].wrap(function _callee7$(_context7) {
                    while (1) {
                      switch (_context7.prev = _context7.next) {
                        case 0:
                          if (!err) {
                            _context7.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context7.abrupt("return");

                        case 4:
                          if (!(result.length == 0)) {
                            _context7.next = 7;
                            break;
                          }

                          resolve({
                            status: 0,
                            message: "Nickname or password incorrect"
                          });
                          return _context7.abrupt("return");

                        case 7:
                          resolve({
                            status: 1,
                            user: result[0]
                          });

                        case 8:
                        case "end":
                          return _context7.stop();
                      }
                    }
                  }, _callee7);
                }));

                return function (_x13, _x14) {
                  return _ref8.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context8.prev = 4;
            _context8.t0 = _context8["catch"](0);
            console.log(_context8.t0);

          case 7:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 4]]);
  }));

  return function (_x12) {
    return _ref7.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.getFollowing = /*#__PURE__*/function () {
  var _ref9 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10(id) {
    return _regenerator["default"].wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;
            return _context10.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT * FROM followers AS f " + "INNER JOIN users AS u ON u.id = f.user_id " + "INNER JOIN users AS us ON us.id = f.follower_id " + "WHERE f.user_id = ? AND us.user_status != 2";
              var value = [id];
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref10 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(err, result) {
                  return _regenerator["default"].wrap(function _callee9$(_context9) {
                    while (1) {
                      switch (_context9.prev = _context9.next) {
                        case 0:
                          if (!err) {
                            _context9.next = 4;
                            break;
                          }

                          console.log(err);
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context9.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result,
                            total: result.length
                          });

                        case 5:
                        case "end":
                          return _context9.stop();
                      }
                    }
                  }, _callee9);
                }));

                return function (_x16, _x17) {
                  return _ref10.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context10.prev = 4;
            _context10.t0 = _context10["catch"](0);
            console.log(_context10.t0);

          case 7:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, null, [[0, 4]]);
  }));

  return function (_x15) {
    return _ref9.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.getFollowers = /*#__PURE__*/function () {
  var _ref11 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee12(id) {
    return _regenerator["default"].wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.prev = 0;
            return _context12.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT * FROM followers AS f " + "INNER JOIN users AS u ON u.id = f.follower_id " + "INNER JOIN users AS us ON us.id = f.user_id " + "WHERE f.follower_id = ? AND us.user_status != 2";
              var value = [id];
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref12 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee11(err, result) {
                  return _regenerator["default"].wrap(function _callee11$(_context11) {
                    while (1) {
                      switch (_context11.prev = _context11.next) {
                        case 0:
                          if (!err) {
                            _context11.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context11.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result,
                            total: result.length
                          });

                        case 5:
                        case "end":
                          return _context11.stop();
                      }
                    }
                  }, _callee11);
                }));

                return function (_x19, _x20) {
                  return _ref12.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context12.prev = 4;
            _context12.t0 = _context12["catch"](0);

          case 6:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12, null, [[0, 4]]);
  }));

  return function (_x18) {
    return _ref11.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.follow = function (user_id, follower_id) {
  try {
    return new Promise(function (resolve, reject) {
      var sql = "INSERT INTO followers (user_id,follower_id) VALUES (?,?)";
      var values = [user_id, follower_id];
      db.query(sql, values, /*#__PURE__*/function () {
        var _ref13 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee13(err, result) {
          return _regenerator["default"].wrap(function _callee13$(_context13) {
            while (1) {
              switch (_context13.prev = _context13.next) {
                case 0:
                  if (!err) {
                    _context13.next = 4;
                    break;
                  }

                  console.log("Error conection db");
                  resolve({
                    status: 0,
                    message: "Error connecion"
                  });
                  return _context13.abrupt("return");

                case 4:
                  resolve({
                    status: 1,
                    data: result[0]
                  });

                case 5:
                case "end":
                  return _context13.stop();
              }
            }
          }, _callee13);
        }));

        return function (_x21, _x22) {
          return _ref13.apply(this, arguments);
        };
      }());
    });
  } catch (error) {
    console.log(error);
  }
};
/* A function that is being exported to be used in another file. */


exports.unfollow = function (user_id, follower_id) {
  try {
    return new Promise(function (resolve, reject) {
      var sql = "DELETE FROM followers WHERE user_id = ? AND follower_id = ?";
      var values = [user_id, follower_id];
      db.query(sql, values, /*#__PURE__*/function () {
        var _ref14 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee14(err, result) {
          return _regenerator["default"].wrap(function _callee14$(_context14) {
            while (1) {
              switch (_context14.prev = _context14.next) {
                case 0:
                  if (!err) {
                    _context14.next = 4;
                    break;
                  }

                  console.log("Error conection db");
                  resolve({
                    status: 0,
                    message: "Error connecion"
                  });
                  return _context14.abrupt("return");

                case 4:
                  resolve({
                    status: 1,
                    data: result[0]
                  });

                case 5:
                case "end":
                  return _context14.stop();
              }
            }
          }, _callee14);
        }));

        return function (_x23, _x24) {
          return _ref14.apply(this, arguments);
        };
      }());
    });
  } catch (error) {
    console.log(error);
  }
};
/* A function that is being exported to be used in another file. */


exports.isFollowing = /*#__PURE__*/function () {
  var _ref15 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee16(user_id, follower_id) {
    return _regenerator["default"].wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            return _context16.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT COUNT(*) AS total FROM followers WHERE user_id = ? AND follower_id = ? ";
              var value = [user_id, follower_id];
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref16 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee15(err, result) {
                  return _regenerator["default"].wrap(function _callee15$(_context15) {
                    while (1) {
                      switch (_context15.prev = _context15.next) {
                        case 0:
                          if (!err) {
                            _context15.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context15.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context15.stop();
                      }
                    }
                  }, _callee15);
                }));

                return function (_x27, _x28) {
                  return _ref16.apply(this, arguments);
                };
              }());
            }));

          case 1:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16);
  }));

  return function (_x25, _x26) {
    return _ref15.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.updateUser = /*#__PURE__*/function () {
  var _ref17 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee18(user) {
    var sql, values;
    return _regenerator["default"].wrap(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            _context18.prev = 0;
            sql = "UPDATE users SET name = ?, nickname = ?, photo = ? ,about_me = ?, tag_id = ? WHERE id = ?";
            values = [user.name, user.nickname, user.photo, user.about_me, user.tag_id, user.id];
            console.log(values);
            return _context18.abrupt("return", new Promise(function (resolve, reject) {
              db.query(sql, values, /*#__PURE__*/function () {
                var _ref18 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee17(err, result) {
                  return _regenerator["default"].wrap(function _callee17$(_context17) {
                    while (1) {
                      switch (_context17.prev = _context17.next) {
                        case 0:
                          if (err) {
                            console.log("Error conection db");
                            resolve({
                              status: 0,
                              message: "Error conection db"
                            });
                          }

                          console.log(result);
                          resolve({
                            status: 1
                          });

                        case 3:
                        case "end":
                          return _context17.stop();
                      }
                    }
                  }, _callee17);
                }));

                return function (_x30, _x31) {
                  return _ref18.apply(this, arguments);
                };
              }());
            }));

          case 7:
            _context18.prev = 7;
            _context18.t0 = _context18["catch"](0);
            console.log(_context18.t0);

          case 10:
          case "end":
            return _context18.stop();
        }
      }
    }, _callee18, null, [[0, 7]]);
  }));

  return function (_x29) {
    return _ref17.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.userExists = /*#__PURE__*/function () {
  var _ref19 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee20(nickname) {
    var sql, value;
    return _regenerator["default"].wrap(function _callee20$(_context20) {
      while (1) {
        switch (_context20.prev = _context20.next) {
          case 0:
            _context20.prev = 0;
            sql = "SELECT nickname FROM users WHERE nickname = ?";
            value = [nickname];
            return _context20.abrupt("return", new Promise(function (resolve, reject) {
              db.query(sql, value, /*#__PURE__*/function () {
                var _ref20 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee19(err, result) {
                  return _regenerator["default"].wrap(function _callee19$(_context19) {
                    while (1) {
                      switch (_context19.prev = _context19.next) {
                        case 0:
                          if (err) {
                            console.log("Error conection db");
                            resolve({
                              status: 0,
                              message: "Error conection db"
                            });
                          }

                          resolve({
                            status: 1,
                            data: result
                          });

                        case 2:
                        case "end":
                          return _context19.stop();
                      }
                    }
                  }, _callee19);
                }));

                return function (_x33, _x34) {
                  return _ref20.apply(this, arguments);
                };
              }());
            }));

          case 6:
            _context20.prev = 6;
            _context20.t0 = _context20["catch"](0);
            console.log(_context20.t0);

          case 9:
          case "end":
            return _context20.stop();
        }
      }
    }, _callee20, null, [[0, 6]]);
  }));

  return function (_x32) {
    return _ref19.apply(this, arguments);
  };
}();
/* A function that is used to search for users in the database. */


exports.getUsersSearch = function (nickname) {
  try {
    var sql = 'SELECT u.id, u.name, u.nickname, u.about_me, u.photo, u.email, t.name as tag FROM users as u ' + 'INNER JOIN tags as t ' + 'ON u.tag_id = t.id ' + 'WHERE nickname REGEXP CONCAT("^", ?) AND u.user_status != 2';
    var value = [nickname];
    return new Promise(function (resolve, reject) {
      db.query(sql, value, /*#__PURE__*/function () {
        var _ref21 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee21(err, result) {
          return _regenerator["default"].wrap(function _callee21$(_context21) {
            while (1) {
              switch (_context21.prev = _context21.next) {
                case 0:
                  if (err) {
                    console.log("Error conection db");
                    resolve({
                      status: 0,
                      message: "Error conection db"
                    });
                  }

                  resolve({
                    status: 1,
                    result: result
                  });

                case 2:
                case "end":
                  return _context21.stop();
              }
            }
          }, _callee21);
        }));

        return function (_x35, _x36) {
          return _ref21.apply(this, arguments);
        };
      }());
    });
  } catch (error) {
    console.log(error);
  }
};
/* A function that is used to get the users from the database. */


exports.getUserByTag = function (tag, index) {
  try {
    var sql = 'SELECT  u.id, u.name, u.nickname, u.about_me, u.photo, u.email, t.name as tag FROM users as u ' + 'INNER JOIN tags as t ' + 'ON u.tag_id = t.id ' + 'WHERE u.tag_id = ? AND u.user_status != 2 ' + 'LIMIT ?,?';
    index = parseInt(index);
    var value = [tag, index, NEXT_X_USERS];

    if (tag == 1) {
      sql = 'SELECT u.id, u.name, u.nickname, u.about_me, u.photo, u.email, t.name as tag FROM users as u ' + 'INNER JOIN tags as t ' + 'ON u.tag_id = t.id ' + 'WHERE u.user_status != 2 ' + 'LIMIT ?,?';
      value = [index, NEXT_X_USERS];
    }

    return new Promise(function (resolve, reject) {
      db.query(sql, value, /*#__PURE__*/function () {
        var _ref22 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee22(err, result) {
          return _regenerator["default"].wrap(function _callee22$(_context22) {
            while (1) {
              switch (_context22.prev = _context22.next) {
                case 0:
                  if (err) {
                    console.log("err");
                    resolve({
                      status: 0,
                      message: "Error conection db"
                    });
                  }

                  resolve({
                    status: 1,
                    result: result
                  });

                case 2:
                case "end":
                  return _context22.stop();
              }
            }
          }, _callee22);
        }));

        return function (_x37, _x38) {
          return _ref22.apply(this, arguments);
        };
      }());
    });
  } catch (error) {
    console.log(error);
  }
};
/* Updating the password of the user. */


exports.updatePassword = function (password, id) {
  try {
    return new Promise(function (resolve, reject) {
      var sql = "UPDATE users SET password = ? WHERE id = ?";
      var values = [password, id];
      db.query(sql, values, function (err, result, fields) {
        if (err) {
          resolve({
            staus: 0,
            message: "Error database"
          });
          return;
        }

        resolve({
          status: 1,
          lastId: result.insertId
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
};
/* Updating the user_status column in the users table. */


exports.changeStatus = function (id, status) {
  try {
    var sql = "UPDATE users SET user_status = ? WHERE id = ?";
    var values = [status, id];
    return new Promise(function (resolve, reject) {
      db.query(sql, values, function (err, result, fields) {
        if (err) {
          resolve({
            staus: 0,
            message: "Error database"
          });
          return;
        }

        resolve({
          status: 1,
          lastId: result.insertId
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
};