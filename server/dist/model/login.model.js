"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var connection = require('../config/db').connection;
/* A function that returns a promise. */


exports.loginGoogle = function (email) {
  try {
    return new Promise(function (resolve, reject) {
      var sql = "SELECT name, nickname, email, id, photo, role_id AS role, user_status FROM users WHERE email = ?";
      var value = [email];
      connection.query(sql, value, /*#__PURE__*/function () {
        var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(err, result) {
          return _regenerator["default"].wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  if (!err) {
                    _context.next = 5;
                    break;
                  }

                  console.log("Error conection db");
                  resolve({
                    status: 0,
                    message: "Error connecion"
                  });
                  return _context.abrupt("return");

                case 5:
                  if (!(result.length == 0)) {
                    _context.next = 10;
                    break;
                  }

                  resolve({
                    status: 0,
                    message: "".concat(email, " no exist")
                  });
                  return _context.abrupt("return");

                case 10:
                  resolve({
                    status: 1,
                    message: "Succesfuly",
                    user: result[0]
                  });

                case 11:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));

        return function (_x, _x2) {
          return _ref.apply(this, arguments);
        };
      }());
    });
  } catch (error) {
    console.log(error);
  }
};