"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var connection = require('../config/db').connection;

var globalFunctions = require('../global/globalFunctions');

var NEXT_X_NEWS = 10;

var cloudingIO = require('../config/clouding.io');
/* A function that is being exported to be used in another file. */


exports.addNews = function (news) {
  try {
    return new Promise(function (resolve, reject) {
      var date = globalFunctions.getDateTypeSQL();
      var sql = "INSERT INTO news (user_id, news_title, subtitle, main_cover ,news_text, creation_date, tag_id) VALUES (?,?,?,?,?,?,?)";
      var values = [news.userId, news.title, news.subTitle, "".concat(cloudingIO.config, "/uploads\\").concat(news.image), news.newsText, date, null];
      connection.query(sql, values, function (err, result, fields) {
        if (err) {
          resolve({
            staus: 0,
            message: "There has been an error when posting the news. Check that the fields are correct."
          });
          console.log(err);
          return;
        }

        resolve({
          status: 1,
          lastId: result.insertId
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
};
/* A function that is being exported to be used in another file. */


exports.editNews = function (news) {
  try {
    var date = globalFunctions.getDateTypeSQL();
    var sql = "UPDATE news SET user_id=?, news_title = ?,subtitle = ?, main_cover = ? ,news_text = ?, creation_date = ?, tag_id = ? WHERE id = ?";
    var values = [news.userId, news.title, news.subTitle, "".concat(cloudingIO.config, "/uploads\\").concat(news.image), news.newsText, date, null, news.id];
    return new Promise(function (resolve, reject) {
      connection.query(sql, values, function (err, result, fields) {
        if (err) {
          resolve({
            staus: 0,
            message: "Error database"
          });
          return;
        }

        resolve({
          status: 1,
          lastId: result
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
};
/* A function that is being exported to be used in another file. */


exports.getNewsById = function (id) {
  try {
    return new Promise(function (resolve, reject) {
      var date = globalFunctions.getDateTypeSQL();
      var sql = "SELECT * FROM news WHERE id = ?";
      var values = [id];
      connection.query(sql, values, function (err, result, fields) {
        if (err) {
          resolve({
            staus: 0,
            message: "Error database"
          });
          return;
        }

        resolve({
          status: 1,
          content: result
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
};
/* A function that is being exported to be used in another file. */


exports.getNewsByUserId = function (id) {
  try {
    return new Promise(function (resolve, reject) {
      var date = globalFunctions.getDateTypeSQL();
      var sql = "SELECT * FROM news WHERE user_id = ? ORDER BY creation_date DESC";
      var values = [id];
      connection.query(sql, values, function (err, result, fields) {
        if (err) {
          resolve({
            staus: 0,
            message: "Error database"
          });
          return;
        }

        resolve({
          status: 1,
          content: result
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
};
/* A function that is being exported to be used in another file. */


exports.getNextXNews = function (index) {
  try {
    return new Promise(function (resolve, reject) {
      var sql = "SELECT n.*, u.nickname, u.photo FROM news AS n INNER JOIN users AS u ON u.id = user_id WHERE u.user_status != 2 LIMIT ?,?";
      var values = [index, NEXT_X_NEWS];
      connection.query(sql, values, function (err, result) {
        if (err) {
          resolve({
            staus: 0,
            message: err
          });
          return;
        }

        resolve({
          status: 1,
          content: result
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
};
/* A function that is being exported to be used in another file. */


exports.save = /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(user_id, news_id) {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            return _context2.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "INSERT INTO saves (user_id,news_id) VALUES (?,?)";
              var values = [user_id, news_id];
              connection.query(sql, values, /*#__PURE__*/function () {
                var _ref2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(err, result) {
                  return _regenerator["default"].wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          if (!err) {
                            _context.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x3, _x4) {
                  return _ref2.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context2.prev = 4;
            _context2.t0 = _context2["catch"](0);
            console.log(_context2.t0);

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 4]]);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.unsave = /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(user_id, news_id) {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            return _context4.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "DELETE FROM saves WHERE user_id = ? AND news_id = ?";
              var values = [user_id, news_id];
              connection.query(sql, values, /*#__PURE__*/function () {
                var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(err, result) {
                  return _regenerator["default"].wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          if (!err) {
                            _context3.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context3.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3);
                }));

                return function (_x7, _x8) {
                  return _ref4.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context4.prev = 4;
            _context4.t0 = _context4["catch"](0);
            console.log(_context4.t0);

          case 7:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[0, 4]]);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.isSaved = /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(user_id, news_id) {
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _context6.prev = 0;
            return _context6.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT COUNT(*) AS total FROM saves WHERE user_id = ? AND news_id = ? ";
              var value = [user_id, news_id];
              connection.query(sql, value, /*#__PURE__*/function () {
                var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(err, result) {
                  return _regenerator["default"].wrap(function _callee5$(_context5) {
                    while (1) {
                      switch (_context5.prev = _context5.next) {
                        case 0:
                          if (!err) {
                            _context5.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context5.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context5.stop();
                      }
                    }
                  }, _callee5);
                }));

                return function (_x11, _x12) {
                  return _ref6.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context6.prev = 4;
            _context6.t0 = _context6["catch"](0);
            console.log(_context6.t0);

          case 7:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[0, 4]]);
  }));

  return function (_x9, _x10) {
    return _ref5.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.like = /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(user_id, news_id) {
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            _context8.prev = 0;
            return _context8.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "INSERT INTO likes (user_id,news_id) VALUES (?,?)";
              var values = [user_id, news_id];
              connection.query(sql, values, /*#__PURE__*/function () {
                var _ref8 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(err, result) {
                  return _regenerator["default"].wrap(function _callee7$(_context7) {
                    while (1) {
                      switch (_context7.prev = _context7.next) {
                        case 0:
                          if (!err) {
                            _context7.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context7.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context7.stop();
                      }
                    }
                  }, _callee7);
                }));

                return function (_x15, _x16) {
                  return _ref8.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context8.prev = 4;
            _context8.t0 = _context8["catch"](0);
            console.log(_context8.t0);

          case 7:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8, null, [[0, 4]]);
  }));

  return function (_x13, _x14) {
    return _ref7.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.unlike = /*#__PURE__*/function () {
  var _ref9 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10(user_id, news_id) {
    return _regenerator["default"].wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.prev = 0;
            return _context10.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "DELETE FROM likes WHERE user_id = ? AND news_id = ?";
              var values = [user_id, news_id];
              connection.query(sql, values, /*#__PURE__*/function () {
                var _ref10 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(err, result) {
                  return _regenerator["default"].wrap(function _callee9$(_context9) {
                    while (1) {
                      switch (_context9.prev = _context9.next) {
                        case 0:
                          if (!err) {
                            _context9.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context9.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context9.stop();
                      }
                    }
                  }, _callee9);
                }));

                return function (_x19, _x20) {
                  return _ref10.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context10.prev = 4;
            _context10.t0 = _context10["catch"](0);
            console.log(_context10.t0);

          case 7:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10, null, [[0, 4]]);
  }));

  return function (_x17, _x18) {
    return _ref9.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.isLiked = /*#__PURE__*/function () {
  var _ref11 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee12(user_id, news_id) {
    return _regenerator["default"].wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.prev = 0;
            return _context12.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT COUNT(*) AS total FROM likes WHERE user_id = ? AND news_id = ? ";
              var value = [user_id, news_id];
              connection.query(sql, value, /*#__PURE__*/function () {
                var _ref12 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee11(err, result) {
                  return _regenerator["default"].wrap(function _callee11$(_context11) {
                    while (1) {
                      switch (_context11.prev = _context11.next) {
                        case 0:
                          if (!err) {
                            _context11.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context11.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context11.stop();
                      }
                    }
                  }, _callee11);
                }));

                return function (_x23, _x24) {
                  return _ref12.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context12.prev = 4;
            _context12.t0 = _context12["catch"](0);
            console.log(_context12.t0);

          case 7:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12, null, [[0, 4]]);
  }));

  return function (_x21, _x22) {
    return _ref11.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.deleteNewsById = /*#__PURE__*/function () {
  var _ref13 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee14(news_id) {
    return _regenerator["default"].wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.prev = 0;
            return _context14.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "DELETE FROM news WHERE id = ?";
              var value = [news_id];
              connection.query(sql, value, /*#__PURE__*/function () {
                var _ref14 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee13(err, result) {
                  return _regenerator["default"].wrap(function _callee13$(_context13) {
                    while (1) {
                      switch (_context13.prev = _context13.next) {
                        case 0:
                          if (!err) {
                            _context13.next = 4;
                            break;
                          }

                          console.log("Error conection db");
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context13.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result[0]
                          });

                        case 5:
                        case "end":
                          return _context13.stop();
                      }
                    }
                  }, _callee13);
                }));

                return function (_x26, _x27) {
                  return _ref14.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context14.prev = 4;
            _context14.t0 = _context14["catch"](0);
            console.log(_context14.t0);

          case 7:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14, null, [[0, 4]]);
  }));

  return function (_x25) {
    return _ref13.apply(this, arguments);
  };
}();
/* A function that is being exported to be used in another file. */


exports.getSavesNews = /*#__PURE__*/function () {
  var _ref15 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee16(user_id) {
    return _regenerator["default"].wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            _context16.prev = 0;
            return _context16.abrupt("return", new Promise(function (resolve, reject) {
              var sql = "SELECT n.* FROM news as n JOIN saves as s ON n.id = s.news_id WHERE s.user_id = ?";
              var value = [user_id];
              connection.query(sql, value, /*#__PURE__*/function () {
                var _ref16 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee15(err, result) {
                  return _regenerator["default"].wrap(function _callee15$(_context15) {
                    while (1) {
                      switch (_context15.prev = _context15.next) {
                        case 0:
                          if (!err) {
                            _context15.next = 4;
                            break;
                          }

                          console.log(err);
                          resolve({
                            status: 0,
                            message: "Error connecion"
                          });
                          return _context15.abrupt("return");

                        case 4:
                          resolve({
                            status: 1,
                            data: result
                          });

                        case 5:
                        case "end":
                          return _context15.stop();
                      }
                    }
                  }, _callee15);
                }));

                return function (_x29, _x30) {
                  return _ref16.apply(this, arguments);
                };
              }());
            }));

          case 4:
            _context16.prev = 4;
            _context16.t0 = _context16["catch"](0);
            console.log(_context16.t0);

          case 7:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16, null, [[0, 4]]);
  }));

  return function (_x28) {
    return _ref15.apply(this, arguments);
  };
}();