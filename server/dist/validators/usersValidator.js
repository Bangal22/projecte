"use strict";

var _require = require('express-validator'),
    body = _require.body,
    check = _require.check,
    validationResult = _require.validationResult;

var validateCreate = [check('nickname').isEmpty(), check('name').isEmpty(), check('email').isEmpty().isEmail(), check('password').isEmpty().isLength({
  min: 8
}).isStrongPassword(), check('confirmPassword').isEmpty().isLength({
  min: 8
}).isStrongPassword()];