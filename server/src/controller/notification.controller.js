const modelNotifications = require('../model/notifications.model');

/* A function that is exported to be used in another file. */
exports.getNotificationsById = async function (req, res){
	let data = await modelNotifications.getNotificationsById(req.params.id);
	res.send(data);
}

/* Exporting a function to be used in another file. */
exports.notificationsViewed = async function (req, res){
	let data = await modelNotifications.notificationsViewed(req.body.userId);
	res.send(data);
}