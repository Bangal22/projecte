// const uuid = require('uuid');
const path = require('path');
const multer  = require('multer');
const express   = require('express');

/* Creating a storage object for multer to use. */
const storage = multer.diskStorage({
  destination : (req, file, cb)=>{
      cb(null, __dirname+'/../uploads')
  },
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null,"d"+uniqueSuffix + path.extname(file.originalname))
 
  }
})
const upload = multer({storage: storage})


module.exports = upload